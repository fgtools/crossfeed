/*\
 * cf_http.hxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifndef _CF_HTTP_HXX_
#define _CF_HTTP_HXX_

#if (!defined(SOCKET) && !defined(_MSC_VER))
typedef int SOCKET;
#endif

////////////////////////////////////////////////////////////////////////////////////
// HTTP PORT
// =========
enum BrType {
    bt_unknown = 0,
    bt_msie,
    bt_chrome,
    bt_firefox,
    bt_ns,
    bt_opera,
    bt_safari,
    bt_moz,
    bt_lynx,
    bt_wget,
    bt_max
};

typedef struct tagBRNAME {
    BrType bt;
    const char *name;
}BRNAME, *PBRNAME;

// HTTP stat storage
typedef struct tagIPCNT {
    unsigned int ip; // note just the IP, as an int, to string by getHostStg(ip)
    int count;
    int i_http_requests;
    int i_http_receives;
    int i_http_wudblk;
    int i_http_errors;
    int i_http_rcvbytes;
    int i_http_senderrs;
    int i_http_sentbytes;
    int bt_count[bt_max];
    double start, last, total;
}IPCNT, *PIPCNT;

typedef std::vector<IPCNT> vIPCNT;

extern int Create_HTTP_Port();
extern void Close_HTTP();
#ifdef USE_MONGOOSE_LIB
extern void http_poll(int timeout_ms);
#else
extern void Deal_With_HTTP( SOCKET fd, netAddress &HTTPAddress );
extern void Add_IP_Counts( std::string &s );
extern netSocket *m_HTTPSocket;
extern vIPCNT vIpCnt;
#endif

extern int get_http_port();
extern void set_http_port( int port );
extern int get_http_recvs();
extern int get_json_reqs();
extern int get_xml_reqs();

extern int m_HTTPReceived;

#ifdef _MSC_VER
#ifndef MSG_NOSIGNAL
  #define MSG_NOSIGNAL 0
#endif
#endif

#endif // #ifndef _CF_HTTP_HXX_
// eof - cf_http.hxx
