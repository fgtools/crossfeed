/*\
 * cf_http.cxx
 *
 * Copyright (c) 2014 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
\*/

#ifdef _MSC_VER
#include <sys/types.h>
#include <sys/stat.h>
#include <WinSock2.h>
#include <Ws2tcpip.h>
#include <direct.h> // getcwd()
#include <time.h>
#else // !_MSC_VER
#include <sys/stat.h> // unix struct stat
#define __STDC_FORMAT_MACROS
#include <inttypes.h> // for PRIu64
#include <unistd.h> // for getcwd
#include "daemon.hxx"
#include "logstream.hxx"
#define stricmp strcasecmp
#endif // _MSC_VER y/n

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdint.h> // for uint64_t
#include <string>
#include <vector>

#include "cf_client.hxx"
#include "cf_misc.hxx"
#include "mpKeyboard.hxx"
#include "mpMsgs.hxx"
#include "cf_pilot.hxx"
#include "netSocket.h"
#include "sprtf.hxx"
#include "cf_ini.hxx"
#ifdef USE_MONGOOSE_LIB
#include "mongoose.h"
#endif
#include "cf_http.hxx"


static const char *mod_name = "cf_http";
#define module mod_name

////////////////////////////////////////////////////////////////////////////////////
// HTTP PORT
// =========

#ifndef HTTP_PORT
#define HTTP_PORT		3335
#endif

static int m_HTTPPort = HTTP_PORT;
int m_HTTPReceived = 0;
static int m_JReceived = 0;
static int m_XReceived = 0;

int get_http_recvs() { return m_HTTPReceived; }
int get_json_reqs() { return m_JReceived; }
int get_xml_reqs() { return m_XReceived; }
int get_http_port() { return m_HTTPPort; }
void set_http_port( int port ) { m_HTTPPort = port; }

#ifdef USE_MONGOOSE_LIB

/////////////////////////////////////////////////////////////////////////////////////////
// the server
// foward refs
void send_extra_headers(struct mg_connection *conn);
void http_close();

static bool use_plain_text = false;
static bool send_exta_hdrs = true;
static char info[] =
    "<!DOCTYPE html>"
    "<html>"
    "<head>"
    "<meta charset=\"utf-8\">"
    "<title>Server Information</title>"
    "</head>"
    "<body>"
    "<h1 align=\"center\">Server Information</h1>"
    "<p>This server only responds to two URI :-</p>"
    "<ul>"
    "<li>/flights.json - Return a json encoded list of current pilots</li>"
    "<li>/flights.xml - The same list xml encoded list of current pilots</li>"
    "<li>/ or /info - Returns this page</li>"
    "</ul>"
    "<p><strong>All others will return 400 bad command, or 404 not found</strong></p>";

static size_t cb_cnt = 0;
static size_t http_cnt = 0;
static size_t json_cnt = 0;
static size_t xml_cnt = 0;
static size_t info_cnt = 0;

char *get_http_stats()
{
    char *cp = GetNxtBuf();
    sprintf(cp, "%s: callbacks %d, http get %d, json %d, xml %d, info %d.", module,
        (int)cb_cnt,
        (int)http_cnt,
        (int)json_cnt,
        (int)xml_cnt,
        (int)info_cnt );
    return cp;
}

static int showOptions(struct mg_connection *conn)
{
    int iret = MG_FALSE;
    char *cp = GetNxtBuf();
    strcpy(cp,info);
    strcat(cp,"<p>");
    strcat(cp,get_http_stats());
    strcat(cp,"</p>\n<p>");
    strcat(cp,get_simple_stats());
    strcat(cp,"</p>");
    strcat(cp,"<p>");
    strcat(cp,get_pilot_stats().c_str());
    strcat(cp,"</p>");
    strcat(cp,"</body></html>\n");
    int len = strlen(cp);
    if (len) {
        mg_send_header(conn,"Content-Type","text/html");
        mg_send_data(conn,cp,len);
        iret = MG_TRUE;
        if (VERB2) SPRTF("%s: Sent Information html, len %d\n", module, len);
    }
    return iret;
}

static int sendJSON(struct mg_connection *conn)
{
    int iret = MG_FALSE;
    char *cp = 0;
    int len = Get_JSON( &cp );
    if (len && cp) {
        if (use_plain_text)
            mg_send_header(conn,"Content-Type","text/plain");
        else
            mg_send_header(conn,"Content-Type","application/json");
        if (send_exta_hdrs)
            send_extra_headers(conn);
        mg_send_data(conn,cp,len);
        iret = MG_TRUE;
        if (VERB2) SPRTF("%s: Sent JSON string, len %d\n", module, len);
    }
    return iret;
}

static int sendXML(struct mg_connection *conn)
{
    int iret = MG_FALSE;
    char *cp = 0;
    int len = Get_XML( &cp );
    if (len && cp) {
        mg_send_header(conn,"Content-Type","text/xml");
        if (send_exta_hdrs)
            send_extra_headers(conn);
        mg_send_data(conn,cp,len);
        iret = MG_TRUE;
        if (VERB2) SPRTF("%s: Sent XML string, len %d\n", module, len);

    }
    return iret;
}


static int event_handler(struct mg_connection *conn, enum mg_event ev) 
{
    double secs = get_seconds();
    int iret = MG_FALSE;
    int i;
    // bool verb = (VERB1) ? true : false;
    const char *q = (conn->query_string && *conn->query_string) ? "?" : "";
    cb_cnt++;
    if (ev == MG_AUTH) {
        return MG_TRUE;   // Authorize all requests
    } else if (ev == MG_REQUEST) {
        http_cnt++;
        m_HTTPReceived++;
        if (VERB5) {
            SPRTF("%s: got URI %s%s%s\n", module,
                conn->uri,q,
                ((q && *q) ? conn->query_string : "") );
            if (VERB9) {
                if (conn->num_headers) {
                    SPRTF("%s: Show of %d headers...\n", module, conn->num_headers);
                    for (i = 0; i < conn->num_headers; i++) {
                        //struct mg_header *p = &conn->http_headers[i];
                        const char *n = conn->http_headers[i].name;
                        const char *v = conn->http_headers[i].value;
                        if (n && v) {
                            SPRTF(" %s: %s\n", n, v );
                        }
                    }
                }
            }
        }
        if ((strcmp(conn->uri,"/") == 0)||(strcmp(conn->uri,"/info") == 0)) {
            info_cnt++;
            iret = showOptions(conn);
        } else if (strcmp(conn->uri,"/flights.json") == 0) {
            json_cnt++;
            iret = sendJSON(conn);
            m_JReceived++;
        } else if (strcmp(conn->uri,"/flights.xml") == 0) {
            xml_cnt++;
            iret = sendXML(conn);
            m_XReceived++;
        } else {
            // iret = sendFile(conn);
        }
    }
    if ( (ev == MG_REQUEST) && (iret == MG_FALSE) ) {
        if (VERB2) {
            SPRTF("%s: No repsonse sent! Returning MG_FALSE to mongoose.\n", module );
        }
    }
    m_dSecs_in_HTTP += (get_seconds() - secs);
    return iret;
}

//////////////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#define snprintf _snprintf
#endif
#ifndef MVER
#define MVER MONGOOSE_VERSION
#endif

static char server_name[64];        // Set by init_server_name()
static struct mg_server *server;    // Set by start_mongoose()

void send_extra_headers(struct mg_connection *conn)
{
    mg_send_header(conn,"Access-Control-Allow-Origin","*");
    mg_send_header(conn,"Access-Control-Allow-Methods","OPTIONS, POST, GET");
    mg_send_header(conn,"Access-Control-Allow-Headers","Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token");
}

#define EV_HANDLER event_handler

static void init_server_name(void)
{
  const char *descr = " for crossfeed project";
  snprintf(server_name, sizeof(server_name), "Mongoose web server v.%s%s",
           MVER, descr);
}


int http_init( int port, mg_handler_t handler )
{
    init_server_name();
    if (handler)
        server = mg_create_server(NULL, handler);
    else
        server = mg_create_server(NULL, EV_HANDLER);
    if (!server) {
        SPRTF("%s: mg_create_server(NULL, event_handler) FAILED!\n", module );
        return 1;
    }
    char *tmp = GetNxtBuf();
    sprintf(tmp,"%u",port);
    const char *msg = mg_set_option(server, "listening_port", tmp);
    if (msg) {
        http_close();
        SPRTF("%s: Failed to set listening port %u - %s!\n", module, port, msg);
        return 1;
    }

    // there is NO document root here mg_get_option(server, "document_root")
    SPRTF("%s: %s on port %s\n", module,
         server_name, 
         mg_get_option(server, "listening_port"));

    return 0;
}

// poll for http events - uses select with ms timeout
void http_poll(int timeout_ms)
{
    if (server) {
        mg_poll_server(server, timeout_ms);
    }
}

// close the http server
void http_close()
{
    if (server)
        mg_destroy_server(&server);
    server = 0;
    SPRTF("%s: destroyed mongoose server.\n", module);
}


//////////////////////////////////////////////////////////////////////////////////


int Create_HTTP_Port()
{
    return http_init( get_http_port(), event_handler );
}
void Close_HTTP()
{
    http_close();
}

#else // !USE_MONGOOSE_LIB

#ifndef MAX_HTTP
#define MAX_HTTP 5
#endif

#ifndef SERVER_ADDRESS
#define SERVER_ADDRESS		"127.0.0.1"
#endif

netSocket *m_HTTPSocket = 0;
static string m_HTTPAddress = SERVER_ADDRESS;
#define MX_HTTP_SIZE 2048

static char http_buf[MX_HTTP_SIZE+2];


std::string get_server_address() { return m_HTTPAddress; }
void set_server_address( std::string addr ) { m_HTTPAddress = addr; }; //m_TelnetAddress = addr; }

/* ------------------------------------------------
    NOTES:
    = GET is the most common HTTP method; it says "give me this resource". 
    = Other methods include POST and HEAD. Method names are always uppercase. 
    = The path is the part of the URL after the host name, also called the 
      request URI (a URI is like a URL, but more general). 
    = The HTTP version always takes the form "HTTP/x.x", uppercase.

    HEADER: Ref: http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
    Example:

GET /data HTTP/1.1
Accept: text/html, application/xhtml+xml, &ast;/&ast
Accept-Language: en-GB
User-Agent: Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0)
UA-CPU: AMD64
Accept-Encoding: gzip, deflate
Host: localhost:3335
Connection: Keep-Alive

    The REPLY
HTTP/1.1 200 OK
Date: Fri, 31 Dec 1999 23:59:59 GMT  - 
Content-Type: text/plain
Content-Length: 42

   ------------------------------------------------ */

typedef std::vector<double> vDBL;
typedef std::vector<std::string> vSTG;

vDBL http_times;
void show_times()
{
    size_t max = http_times.size();
    if (!max)
        return;
    size_t ii;
    double total, d;
    total = 0.0;
    for( ii = 0; ii < max; ii++ ) {
        d = http_times[ii];
        total += d;
    }
    d = total / (double)max;
    SPRTF("Average HTTP GET time %s\n", get_seconds_stg(d));

}

static BRNAME BrName[] = {
    { bt_unknown, "Unknown" },
    { bt_msie, "MSIE" },
    { bt_chrome, "Chrome" },
    { bt_firefox, "Firefox" },
    { bt_ns, "Netscape" },
    { bt_opera, "Opera" },
    { bt_safari, "Safari" },
    { bt_moz, "Mozilla" },
    { bt_lynx, "Lynx" },
    { bt_wget, "Wget" },

    // always LAST
    { bt_max, 0 }
};

const char *get_BrType_Name( BrType bt )
{
    PBRNAME pbn = &BrName[0];
    while( pbn->name ) {
        if (pbn->bt == bt)
            return pbn->name;
        pbn++;
    }
    return "Unknown";
}

//static int redo_select = 0;
//static int send_ok_if_send = 0;
//static int reselect_max_count = 50;
//static int use_nanosleep = 1;
// 1 sec = 1,000,000,000 nanoseconds
//static int nano_wait = 100000000; // just 100 ms
//static int nano_wait = 200000000; // just 200 ms
const char *tn_ok = "HTTP/1.0 200 OK\r\n"
    "Content-Type: text/plain\r\n"
    "\r\n";
const char *tn_nf = "HTTP/1.0 404 Not Found\r\n\r\n";
const char *tn_na = "HTTP/1.0 405 Method Not Allowed\r\n"
    "Allow : GET\r\n"
    "\r\n";

const char *tn_ok1 = "HTTP/1.1 200 OK\r\n"
    "Date: %s GMT\r\n"
    "Content-Type: text/plain\r\n"
    "Content-Length: %d\r\n"
    "Cache-Control: no-cache\r\n"
    "Connection: close\r\n"
    "\r\n";

const char *xml_ok1 = "HTTP/1.1 200 OK\r\n"
    "Content-Type: text/xml\r\n"
    "Content-Length: %d\r\n"
    "Cache-Control: no-cache\r\n"
    "Access-Control-Allow-Origin: *\r\n"
    "\r\n";
const char *xml_ok = "HTTP/1.0 200 OK\r\n"
    "Content-Type: text/xml\r\n"
    "\r\n";


static cf_String out_buf;

vIPCNT vIpCnt;

void out_ip_stats()
{
    size_t max, ii;
    max = vIpCnt.size();
    PIPCNT pip;
    std::string s;
    for (ii = 0; ii < max; ii++) {
        pip = &vIpCnt[ii];
        if (pip->i_http_requests) {
            s = getHostStg( pip->ip );
            SPRTF("HTTP %s: rq=%d, rcv:%d, wb=%d, err=%d, rb=%d, se=%d, sb=%d",
                s.c_str(),
                pip->i_http_requests, pip->i_http_receives, pip->i_http_wudblk, pip->i_http_errors,
                pip->i_http_rcvbytes, pip->i_http_senderrs, pip->i_http_sentbytes );
            if ((pip->count > 5) && (pip->last > pip->start)) {
                double diff = pip->last - pip->start;
                if (diff > 1.0) {
                    double rps = (double)pip->count / diff;
                    SPRTF(", rps=%.3f",rps);
                }
            }
            SPRTF("\n");
        }
    }
}

// got the User-Agent string,
// attempt to get browser type
BrType Get_Browser_Type(char *prot)
{
    BrType bt = bt_unknown;
    if (InStr(prot,(char *)"MSIE"))
        bt = bt_msie;
    else if (InStr(prot,(char *)"Chrome"))
        bt = bt_opera;
    else if (InStr(prot,(char *)"Opera"))
        bt = bt_opera;
    else if (InStr(prot,(char *)"Safari"))
        bt = bt_safari;
    else if (InStr(prot,(char *)"Navigator"))
        bt = bt_ns;
    else if (InStr(prot,(char *)"Firefox"))
        bt = bt_firefox;
    else if (InStr(prot,(char *)"Lynx"))
        bt = bt_lynx;
    else if (InStr(prot,(char *)"Wget"))
        bt = bt_wget;
    else if (InStr(prot,(char *)"Mozilla"))
        bt = bt_moz;

    return bt;
}

// FIX20130404 - Add XML feed
// xml  - URL /flights.xml
// json - URL /flights.json
// retained: send 'OK' + {json} if it is a 'GET ' + '/data '
void Handle_HTTP_Receive( netSocket &NewHTTP, netAddress &HTTPAddress,
    char *buffer, int length, PIPCNT pip )
{
    char *cp = buffer;
    int res = length;
    bool no_uri = false;
    bool is_one = false;
    bool send_json = false;
    bool send_info = false;
    bool send_ok = false;
    bool send_xml = false;
    int c;
    char *prot;
    BrType bt = bt_unknown;
    if (VERB5) SPRTF("[v5] HTTP received %d bytes\n",res);
    if (VERB9) {
        if ( (res + 16) > M_MAX_SPRTF ) {
            SPRTF("[v9] [");
            direct_out_it(cp);
            SPRTF("]\n");
        } else
            SPRTF("[v9] [%s]\n",cp);
    }
    pip->i_http_rcvbytes += res; // add to RECEIVED bytes
    // at present ONLY get the FIRST LINE, but (one day)
    // could parse the WHOLE http reaceive, perhaps especially looking for
    // say 'Connection: Keep-Alive', but at present IGNORE ALL
    while ((c = *cp) != 0) {
        if (( c == '\r' ) || ( c == '\n' )) {
            *cp = 0;    // send ZERO
            cp++;
            while (*cp && (*cp <= ' ')) cp++;
            break;
        }
        cp++;
    }
    if ((c = InStr(cp,(char *)"User-Agent:")) != 0) {
        // found User Agent - note assumes PLUS a space
        prot = (cp + c + 11);
        cp = prot;   // from here to end of this line
        while ((c = *cp) != 0) {
            if (( c == '\r' ) || ( c == '\n' )) {
                *cp = 0;
                break;
            }
            cp++;
        }
        bt = Get_Browser_Type(prot);
    }
    pip->bt_count[bt]++;    // add to this type count

    ////////////////////////
    // Deal with RECEIVE
    ////////////////////////
    cp = buffer;    // back to start
    while (*cp && (*cp <= ' ')) cp++;   // eat any leading spaces
    if (strncmp(cp,"GET ",4) == 0) {
        send_ok = true; // send OK, if nothing else
        cp += 4;
        while (*cp && (*cp <= ' ')) cp++;   // eat any leading spaces
        prot = cp;
        if (strncmp(prot,"HTTP/1.",7) == 0) {
            *prot = 0;
            prot += 7;
            if (*prot == '1')
                is_one = true;
            no_uri = true;
            *prot = 0;
        } else {
            while (*prot && (*prot > ' ')) prot++; // get over URI
            while (*prot && (*prot <= ' ')) prot++; // get over any spaces
            if (strncmp(prot,"HTTP/1.",7) == 0) {
                *prot = 0;
                prot += 7;
                if (*prot == '1')
                    is_one = true;
                *prot = 0;
            }
        }
        // parse URI
        if ((strncmp(cp,"/flights.json",13) == 0) && ((cp[13] <= ' ') || (cp[13] == '?')) ) {
            send_json = true;
            if (VERB5) SPRTF("[v5] HTTP GET /flights.json\n",res);
        } else if ((strncmp(cp,"/flights.xml",12) == 0) && (cp[12] <= ' ')) {
            send_xml = true;
            if (VERB5) SPRTF("[v5] HTTP GET /flights.xml\n",res);
        } else if (no_uri || ((strncmp(cp,"/",1) == 0) && (cp[1] <= ' '))) {
            send_info = true;
            if (VERB5) SPRTF("[v5] HTTP GET %s\n",(no_uri ? "<NoURI>" : "/"));
        } else if ((strncmp(cp,"/data",5) == 0) && (cp[5] <= ' ')) {
            send_json = true;
            if (VERB5) SPRTF("[v5] HTTP GET /data\n",res);
        } else if ((strncmp(cp,"/info",5) == 0) && (cp[5] <= ' ')) {
            send_info = true;
            if (VERB5) SPRTF("[v5] HTTP GET /info\n",res);
        } else {
            if (VERB9) SPRTF("[v9] Got Unexpected GET [%s]\n", cp);
        }
    } else {
        if (VERB9) SPRTF("[v9] Got unparsed request. [%s]\n", cp);
    }

    ///////////////////////
    // Deal with SEND
    ///////////////////////
    char *pbuf = 0;
    int len = 0;
    if (send_json) {
        m_JReceived++;
        len = Get_JSON( &pbuf );
        if (len && pbuf) {
            if (is_one) {
                out_buf.Printf(tn_ok1,get_gmt_stg(),len);
                out_buf.Strcat(pbuf);
                len = (int)out_buf.Strlen();
                res = NewHTTP.send ( out_buf.Str(), len, MSG_NOSIGNAL );
                if (res < 0) {
                    pip->i_http_senderrs++;
                } else {
                    pip->i_http_sentbytes += res;
                }
            } else {
                out_buf.Printf(tn_ok);
                out_buf.Strcat(pbuf);
                res = NewHTTP.send ( out_buf.Str(), (int)out_buf.Strlen(), MSG_NOSIGNAL );
                if (res < 0) {
                    pip->i_http_senderrs++;
                } else {
                    pip->i_http_sentbytes += res;
                }
            }
            if (res < 0) {
                PERROR("HTTP send failed!");
            } else {
                if (VERB9) {
                    if (( out_buf.Strlen() + 40 ) > M_MAX_SPRTF) {
                        SPRTF("[v9] Sent %d bytes [", out_buf.Strlen() );
                        direct_out_it( (char *)out_buf.Str() );
                        SPRTF("]%d\n",res);
                    } else 
                        SPRTF("[v9] Sent %d bytes [%s]%d\n", (int)out_buf.Strlen(), out_buf.Str(), res );
                }
            }
        } else {
            res = NewHTTP.send ( tn_nf, (int)strlen(tn_nf), MSG_NOSIGNAL ); //FILE NOT FOUND
            if (res < 0) {
                pip->i_http_senderrs++;
            } else {
                pip->i_http_sentbytes += res;
            }
            if (VERB9) SPRTF("[v9] No JSON to send. Sent 404 %d\n");
        }
    } else if (send_xml) {
        m_XReceived++;
        len = Get_XML( &pbuf );
        if (len && pbuf) {
            if (is_one) {
                out_buf.Printf(xml_ok1,len);
                out_buf.Strcat(pbuf);
                len = (int)out_buf.Strlen();
                res = NewHTTP.send ( out_buf.Str(), len, MSG_NOSIGNAL );
                if (res < 0) {
                    pip->i_http_senderrs++;
                } else {
                    pip->i_http_sentbytes += res;
                }
            } else {
                out_buf.Printf(xml_ok);
                out_buf.Strcat(pbuf);
                res = NewHTTP.send ( out_buf.Str(), (int)out_buf.Strlen(), MSG_NOSIGNAL );
                if (res < 0) {
                    pip->i_http_senderrs++;
                } else {
                    pip->i_http_sentbytes += res;
                }
            }
            if (res < 0) {
                PERROR("HTTP send failed!");
            } else {
                if (VERB9) {
                    if (( out_buf.Strlen() + 40 ) > M_MAX_SPRTF) {
                        SPRTF("[v9] Sent %d bytes [", out_buf.Strlen() );
                        direct_out_it( (char *)out_buf.Str() );
                        SPRTF("]%d\n",res);
                    } else 
                        SPRTF("[v9] Sent %d bytes [%s]%d\n", (int)out_buf.Strlen(), out_buf.Str(), res );
                }
            }
        } else {
            res = NewHTTP.send ( tn_nf, (int)strlen(tn_nf), MSG_NOSIGNAL ); //FILE NOT FOUND
            if (res < 0) {
                pip->i_http_senderrs++;
            } else {
                pip->i_http_sentbytes += res;
            }
            if (VERB9) SPRTF("[v9] No JSON to send. Sent 404 %d\n");
        }

    } else if (send_info) {
        std::string s = get_info_json();
        len = (int)s.size();
        out_buf.Printf(tn_ok1,get_gmt_stg(),len);
        out_buf.Strcat(s.c_str());
        res = NewHTTP.send ( out_buf.Str(), (int)out_buf.Strlen(), MSG_NOSIGNAL );
        if (res < 0) {
            pip->i_http_senderrs++;
        } else {
            pip->i_http_sentbytes += res;
        }
        if (VERB9) SPRTF("[v9] send info [%s] res = %d\n", out_buf.Str(),res);
    } else if (send_ok) {
        res = NewHTTP.send ( tn_nf, (int)strlen(tn_nf), MSG_NOSIGNAL );
        if (VERB9) SPRTF("[v9] HTTP sent 404 Not Found.(%d)\n", res);
        if (res < 0) {
            pip->i_http_senderrs++;
        } else {
            pip->i_http_sentbytes += res;
        }
    } else {
        res = NewHTTP.send ( tn_na, (int)strlen(tn_na), MSG_NOSIGNAL ); //FILE NOT FOUND
        if (VERB9) SPRTF("[v9] HTTP sent 405 Method Not Allowed %d\n", res);
        if (res < 0) {
            pip->i_http_senderrs++;
        } else {
            pip->i_http_sentbytes += res;
        }
    }
}

PIPCNT Add_to_IP_Count( netAddress &HTTPAddress, double secs )
{
    IPCNT ipcnt;
    PIPCNT pip;
    unsigned int ip = HTTPAddress.getIP();
    size_t max, ii;
    max = vIpCnt.size();
    for (ii = 0; ii < max; ii++) {
        pip = &vIpCnt[ii];
        if (ip == pip->ip) {
            pip->count++;
            pip->last = secs;
            return pip;
        }
    }
    memset(&ipcnt,0,sizeof(IPCNT));
    ipcnt.ip = ip;
    ipcnt.count = 1;
    ipcnt.start = secs;
    ipcnt.total = 0.0;
    vIpCnt.push_back(ipcnt);
    return &vIpCnt[vIpCnt.size() - 1];
}

std::string get_br_string(PIPCNT pip)
{
    std::string s;
    int i;
    for (i = 0; i < bt_max; i++) {
        if (pip->bt_count[i]) {
            if (s.size())
                s += ",";
            s += "\"";
            s += get_BrType_Name((BrType)i);
            s += "\"";
        }
    }
    return s;
}

// add "ips" object array, with "ip":"addr","cnt":"num"
void Add_IP_Counts( std::string &s )
{
    char _buf[256];
    std::string s2;
    PIPCNT pip;
    size_t max, ii;
    max = vIpCnt.size();
    char *cp = _buf;
    for (ii = 0; ii < max; ii++) {
        pip = &vIpCnt[ii];
        s2 = getHostStg(pip->ip); 
        sprintf(cp, "\t{\"ip\":\"%s\",\"cnt\":%d,\"br\":[%s]",
            s2.c_str(), pip->count,
            get_br_string(pip).c_str());

        if (ii) s += ",\n";
        s += cp;
        if ((pip->count > 5) && (pip->last > pip->start)) {
            double diff = pip->last - pip->start;
            if (diff > 1.0) {
                double rps = (double)pip->count / diff;
                sprintf(cp,",\"rps\":%.3f",rps);
                s += cp;
            }
        }
        s += "}"; // close stats for this IP
    }
}

void Deal_With_HTTP( SOCKET fd, netAddress &HTTPAddress )
{
    int res;
    double sec1, sec2, diff;
    sec1 = get_seconds();
    netSocket NewHTTP;
    PIPCNT pip;
    NewHTTP.setHandle (fd);
    pip = Add_to_IP_Count(HTTPAddress, sec1);
    pip->i_http_requests++;
    if (VERB9) {
        string s = HTTPAddress.getHost();
        SPRTF("[v9] Acceped HTTP connection from [%s:%d]\n", s.c_str(), HTTPAddress.getPort());
    }
    res = NewHTTP.recv( http_buf, MX_HTTP_SIZE, 0 ); // get request
    if (res > 0) {
        http_buf[res] = 0; // ensure ZERO termination
        pip->i_http_receives++;
        Handle_HTTP_Receive( NewHTTP, HTTPAddress, http_buf, res, pip );
    } else if (NewHTTP.isNonBlockingError() ) {
        // just a would block error
        pip->i_http_wudblk++;
    } else {
        PERROR("HTTP recv error!");
        pip->i_http_errors++;
    }
    NewHTTP.close();
    sec2 = get_seconds();
    diff = sec2 - sec1;
    if (VERB9) SPRTF("Serviced in %s\n", get_seconds_stg(diff));
    http_times.push_back(diff);
    m_dSecs_in_HTTP += diff;
    pip->total += diff;
}


//////////////////////////////////////////////////////////////////////////////
void Close_HTTP()
{
    if (m_HTTPSocket) {
        m_HTTPSocket->close();
        delete m_HTTPSocket;
        m_HTTPSocket = 0;
    }
}

////////////////////////////////////////////////////////////////////////
int Create_HTTP_Port()
{
    m_HTTPSocket = new netSocket;
    if (m_HTTPSocket->open (true) == 0)  { // TCP-Socket
        SPRTF("%s: ERROR: failed to create HTTP socket\n", mod_name);
        return 1;
    }
    //if (add_non_blocking)
        m_HTTPSocket->setBlocking (false);
    m_HTTPSocket->setSockOpt (SO_REUSEADDR, true);
    if (m_HTTPSocket->bind (m_HTTPAddress.c_str(), m_HTTPPort) != 0) {
        SPRTF("%s: ERROR: failed to bind to HTTP port %d!\n", mod_name, m_HTTPPort);
        return 1;
    }
    if (m_HTTPSocket->listen (MAX_HTTP) != 0) {
        SPRTF("%s: ERROR: failed to listen to HTTP port\n", mod_name);
        return 1;
    }
    return 0;
} 

#endif // USE_MONGOOSE_LIB y/n

// END HTTP PORT
////////////////////////////////////////////////////////////////////////////////////

// eof = cf_http.cxx
