/*\
 * cf_log.cxx
 *
 * Copyright (c) 2015 - Geoff R. McLane
 * Licence: GNU GPL version 2
 *
 * Read and show a crossfeed raw log
 *
\*/
#ifdef _MSC_VER
#pragma warning( disable : 4996 )
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h> // for strdup(), ...
#include <stdint.h>
#include <string>
#include <time.h>
#include "mpMsgs.hxx"
#include "tiny_xdr.hxx"
#include "sprtf.hxx"
#ifdef USE_SIMGEAR  // include SG headers
#include <simgear/compiler.h>
#include <simgear/math/SGMath.hxx>
#include <simgear/math/sg_geodesy.hxx>
#include <simgear/props/props.hxx>
#else // !USE_SIMGEAR
#include "fg_geometry.hxx"
#include "SGMath2.hxx"
#include "cf_euler.hxx"
#endif // USE_SIMGEAR y/n
#include "cf_pilot.hxx"

static const char *module = "cf_log";
#define mod_name module

static const char *usr_input = 0;
    //char *tf = (char *)"cf_raw2.log";
#ifdef _MSC_VER
const char *raw_log = (char *)"C:\\Users\\user\\Downloads\\logs\\fgx-cf\\cf_raw.log";
#else
const char *raw_log = (char *)"/home/geoff/downloads/cf_raw.log";
#endif
const char *def_log = "tempcflog.txt";
static int verbosity = 1;
static bool add_properies = true;
static bool save_properies = false;

#define VERB1 (verbosity >= 1)
#define VERB2 (verbosity >= 2)
#define VERB5 (verbosity >= 5)
#define VERB9 (verbosity >= 9)

#define ISUA(a)  ((a >= 'A') && (a <= 'Z'))
#define ISLA(a)  ((a >= 'a') && (a <= 'z'))
#define ISNUM(a) ((a >= '0') && (a <= '9'))
#define ISSPL(a) ((a == '-') || (a == '_'))

//bool m_Modify_CALLSIGN = true;  // Do need SOME modification (for SQL and json)
bool m_Modify_AIRCRAFT = false;

static int unchanged_int_cnt = 0;
static int unchanged_flt_cnt = 0;
static int unchanged_stg_cnt = 0;

void give_help( char *name )
{
    SPRTF("\n");
    SPRTF("%s: usage: [options] usr_input\n", module);
    SPRTF("Options:\n");
    SPRTF(" --help  (-h or -?) = This help and exit(2)\n");
    SPRTF(" --verb[n]     (-v) = Bump, or set verbosity. (def=%d)\n", verbosity );
    SPRTF("\n");
    // TODO: More help
}

int parse_args( int argc, char **argv )
{
    int i,i2,c;
    char *arg, *sarg;
    for (i = 1; i < argc; i++) {
        arg = argv[i];
        i2 = i + 1;
        if (*arg == '-') {
            sarg = &arg[1];
            while (*sarg == '-')
                sarg++;
            c = *sarg;
            switch (c) {
            case 'h':
            case '?':
                give_help(argv[0]);
                return 2;
                break;
            case 'v':
                sarg++; // skip the -v
                if (*sarg) {
                    if (*sarg == 'v') {
                        verbosity++; /* one inc for first */
                        while(*sarg == 'v') {
                            verbosity++;
                            arg++;
                        }
                    } else {
                        while (*sarg && !ISNUM(*sarg))
                            sarg++;
                        if (ISNUM(*sarg)) {
                            verbosity = atoi(sarg);
                        }
                    }
                } else {
                    verbosity++;
                }
                if (VERB1) printf("%s: Set verbosity to %d\n", mod_name, verbosity);
                break;
            // TODO: Other arguments
            default:
                SPRTF("%s: Unknown argument '%s'. Try -? for help...\n", module, arg);
                return 1;
            }
        } else {
            // bear argument
            if (usr_input) {
                SPRTF("%s: Already have input '%s'! What is this '%s'?\n", module, usr_input, arg );
                return 1;
            }
            usr_input = strdup(arg);
        }
    }
#ifndef NDEBUG
    if (!usr_input) {
        usr_input = strdup(raw_log);
    }
#endif
    if (!usr_input) {
        give_help(argv[0]);
        SPRTF("%s: Error: No user input found in command!\n", module);
        return 1;
    }
    return 0;
}

typedef struct tagPKTS {
    char *bgn;
    int cnt;
}PKTS, *PPKTS;

#ifndef MAX_BUFFER_SIZE
#define MAX_BUFFER_SIZE 4096    // was only 1200???
#endif
#define MAX_TEXT_SIZE 128
#define MAX_PACKET_SIZE 1200

///////////////////////////////////////////////////////////////////////////
enum Pilot_Type {
    pt_Unknown,
    pt_New,
    pt_Revived,
    pt_Pos,
    pt_Expired,
    pt_Stat
};

#ifdef USE_SIMGEAR
enum { X, Y, Z };
enum { Lat, Lon, Alt };
#endif

typedef std::vector<uint32_t> vUint32;

static vUint32 vBadIds;

int add_to_vector( uint32_t id )
{
    size_t ii, max = vBadIds.size();
    for (ii = 0; ii < max; ii++) {
        if (vBadIds[ii] == id) {
            return 0;
        }
    }
    vBadIds.push_back(id);
    return 1;
}

///////////////////////////////////////////////////////////////////////////////
// Pilot information kept in vector list
// =====================================
// NOTE: From 'simgear' onwards, ALL members MUST be value type
// That is NO classes, ie no reference types
// This is updated in the vector by a copy through a pointer - *pp2 = *pp;
// If kept as 'value' members, this is a blindingly FAST rep movsb esi edi;
// *** Please KEEP it that way! ***
typedef struct tagCF_Pilot {
    uint64_t flight_id; // unique flight ID = epoch*1000000+tv_usec
    Pilot_Type      pt;
    bool            expired;
    time_t          curr_time, prev_time, first_time;    // rough seconds
    double          sim_time, prev_sim_time, first_sim_time; // sim time from packet
    char            callsign[MAX_CALLSIGN_LEN];
    char            aircraft[MAX_MODEL_NAME_LEN];
    double          lat, lon;    // degrees
    double          alt;         // feet
#ifdef USE_SIMGEAR  // TOCHECK - new structure members
    double          px, py, pz;
    double          ppx, ppy, ppz;
#else // #ifdef USE_SIMGEAR
    Point3D         SenderPosition;
    Point3D         PrevPos;
    Point3D         SenderOrientation;
    Point3D         GeodPoint;
    Point3D  linearVel, angularVel,  linearAccel, angularAccel;      
#endif // #ifdef USE_SIMGEAR y/n
    int SenderAddress, SenderPort;
    int             packetCount, packetsDiscarded;
    double          heading, pitch, roll, speed;
    double          dist_m; // vector length since last - meters
    double          total_nm, cumm_nm;   // total distance since start
    time_t          exp_time;    // time expired - epoch secs
    time_t          last_seen;  // last packet seen - epoch secs
    void *          pvprops;
    int             unchanged_int_cnt,unchanged_flt_cnt,unchanged_stg_cnt;
    int             max_props, pkt_count;
}CF_Pilot, *PCF_Pilot;

typedef std::vector<CF_Pilot> vCFP;

static vCFP vPilots;


// ==================================================
///////////////////////////////////////////////////////////////////////////
typedef struct tagPKTSTR {
    Packet_Type pt;
    const char *desc;
    int count;
    int totals;
    void *vp;
}PKTSTR, *PPKTSTR;

static PKTSTR sPktStr[pkt_Max] = {
    { pkt_Invalid, "Invalid",     0, 0, 0 },
    { pkt_InvLen1, "InvLen1",     0, 0, 0 },
    { pkt_InvLen2, "InvLen2",     0, 0, 0 },
    { pkt_InvMag, "InvMag",       0, 0, 0 },
    { pkt_InvProto, "InvPoto",    0, 0, 0 },
    { pkt_InvPos, "InvPos",       0, 0, 0 },
    { pkt_InvHgt, "InvHgt",       0, 0, 0 },
    { pkt_InvStg1, "InvCallsign", 0, 0, 0 },
    { pkt_InvStg2, "InvAircraft", 0, 0, 0 },
    { pkt_First, "FirstPos",      0, 0, 0 },
    { pkt_Pos, "Position",        0, 0, 0 },
    { pkt_Chat, "Chat",           0, 0, 0 },
    { pkt_Other, "Other",         0, 0, 0 },
    { pkt_Discards, "Discards",   0, 0, 0 }
};

PPKTSTR Get_Pkt_Str()
{
    return &sPktStr[0];
}

void Clear_Packet_Stats()
{
    int i;
    PPKTSTR pps = &sPktStr[0];
    for (i = 0; i < pkt_Max; i++) {
        pps[i].count = 0;
        if ( pps[i].vp )
            free (pps[i].vp);
        pps[i].vp = 0;
    }
#if 0 // 000000000000000000000000000000000000000000
    if (pvStgs) {
        pvStgs->clear();
        delete pvStgs;
    }
    pvStgs = 0;
    if (pvInvStg1) {
        pvInvStg1->clear();
        delete pvInvStg1;
    }
    pvInvStg1 = 0;
    if (pvInvStg2) {
        pvInvStg2->clear();
        delete pvInvStg2;
    }
    pvInvStg2 = 0;
    if (pvCallsigns) {
        pvCallsigns->clear();
        delete pvCallsigns;
    }
    pvCallsigns = 0;
#endif // 000000000000000000000000000000000000000000
} 


//////////////////////////////////////////////////////////////////////
// Filter CALLSIGN to ONLY ALPHA-NUMERIC (English) characters
// 20130105 - allow lowercase, and '-' or '_'
#define ISOK(a) (ISUA(a) || ISLA(a) || ISNUM(a) || ISSPL(a)) 

static char *get_CallSign( char *pcs )
{
    static char _s_callsign[MAX_CALLSIGN_LEN+2];
    int i, c, off;
    char *cp = _s_callsign;
    off = 0;
    for (i = 0; i < MAX_CALLSIGN_LEN; i++) {
        c = pcs[i];
        if (!c) break; // end on a null
        if (ISOK(c)) { // is acceptable char
            cp[off++] = (char)c;
        }
    }
    cp[off] = 0; // ensure ZERO termination
    return cp;
}

//////////////////////////////////////////////////////////////////////
// Rather rough service to remove leading PATH
// and remove trailing file extension
static char *get_Model( char *pm )
{
    static char _s_buf[MAX_MODEL_NAME_LEN+4];
    int i, c, len;
    char *cp = _s_buf;
    char *model = pm;
    len = MAX_MODEL_NAME_LEN;
    for (i = 0; i < len; i++) {
        c = pm[i];
        if (c == '/')
            model = &pm[i+1];
        else if (c == 0)
            break;
    }
    strcpy(cp,model);
    len = (int)strlen(cp);
    model = 0;
    for (i = 0; i < len; i++) {
        c = cp[i];
        if (c == '.')
            model = &cp[i];
    }
    if (model)
        *model = 0;
    return cp;
}

struct IdPropertyList {
  unsigned id;
  const char* name;
  simgear::props::Type type;
};

// A static map of protocol property id values to property paths,
// This should be extendable dynamically for every specific aircraft ...
// For now only that static list
static const IdPropertyList sIdPropertyList[] = {
  {100, "surface-positions/left-aileron-pos-norm",  simgear::props::FLOAT},
  {101, "surface-positions/right-aileron-pos-norm", simgear::props::FLOAT},
  {102, "surface-positions/elevator-pos-norm",      simgear::props::FLOAT},
  {103, "surface-positions/rudder-pos-norm",        simgear::props::FLOAT},
  {104, "surface-positions/flap-pos-norm",          simgear::props::FLOAT},
  {105, "surface-positions/speedbrake-pos-norm",    simgear::props::FLOAT},
  {106, "gear/tailhook/position-norm",              simgear::props::FLOAT},
  {107, "gear/launchbar/position-norm",             simgear::props::FLOAT},
  {108, "gear/launchbar/state",                     simgear::props::STRING},
  {109, "gear/launchbar/holdback-position-norm",    simgear::props::FLOAT},
  {110, "canopy/position-norm",                     simgear::props::FLOAT},
  {111, "surface-positions/wing-pos-norm",          simgear::props::FLOAT},
  {112, "surface-positions/wing-fold-pos-norm",     simgear::props::FLOAT},

  {200, "gear/gear[0]/compression-norm",           simgear::props::FLOAT},
  {201, "gear/gear[0]/position-norm",              simgear::props::FLOAT},
  {210, "gear/gear[1]/compression-norm",           simgear::props::FLOAT},
  {211, "gear/gear[1]/position-norm",              simgear::props::FLOAT},
  {220, "gear/gear[2]/compression-norm",           simgear::props::FLOAT},
  {221, "gear/gear[2]/position-norm",              simgear::props::FLOAT},
  {230, "gear/gear[3]/compression-norm",           simgear::props::FLOAT},
  {231, "gear/gear[3]/position-norm",              simgear::props::FLOAT},
  {240, "gear/gear[4]/compression-norm",           simgear::props::FLOAT},
  {241, "gear/gear[4]/position-norm",              simgear::props::FLOAT},

  {300, "engines/engine[0]/n1",  simgear::props::FLOAT},
  {301, "engines/engine[0]/n2",  simgear::props::FLOAT},
  {302, "engines/engine[0]/rpm", simgear::props::FLOAT},
  {310, "engines/engine[1]/n1",  simgear::props::FLOAT},
  {311, "engines/engine[1]/n2",  simgear::props::FLOAT},
  {312, "engines/engine[1]/rpm", simgear::props::FLOAT},
  {320, "engines/engine[2]/n1",  simgear::props::FLOAT},
  {321, "engines/engine[2]/n2",  simgear::props::FLOAT},
  {322, "engines/engine[2]/rpm", simgear::props::FLOAT},
  {330, "engines/engine[3]/n1",  simgear::props::FLOAT},
  {331, "engines/engine[3]/n2",  simgear::props::FLOAT},
  {332, "engines/engine[3]/rpm", simgear::props::FLOAT},
  {340, "engines/engine[4]/n1",  simgear::props::FLOAT},
  {341, "engines/engine[4]/n2",  simgear::props::FLOAT},
  {342, "engines/engine[4]/rpm", simgear::props::FLOAT},
  {350, "engines/engine[5]/n1",  simgear::props::FLOAT},
  {351, "engines/engine[5]/n2",  simgear::props::FLOAT},
  {352, "engines/engine[5]/rpm", simgear::props::FLOAT},
  {360, "engines/engine[6]/n1",  simgear::props::FLOAT},
  {361, "engines/engine[6]/n2",  simgear::props::FLOAT},
  {362, "engines/engine[6]/rpm", simgear::props::FLOAT},
  {370, "engines/engine[7]/n1",  simgear::props::FLOAT},
  {371, "engines/engine[7]/n2",  simgear::props::FLOAT},
  {372, "engines/engine[7]/rpm", simgear::props::FLOAT},
  {380, "engines/engine[8]/n1",  simgear::props::FLOAT},
  {381, "engines/engine[8]/n2",  simgear::props::FLOAT},
  {382, "engines/engine[8]/rpm", simgear::props::FLOAT},
  {390, "engines/engine[9]/n1",  simgear::props::FLOAT},
  {391, "engines/engine[9]/n2",  simgear::props::FLOAT},
  {392, "engines/engine[9]/rpm", simgear::props::FLOAT},

  {800, "rotors/main/rpm", simgear::props::FLOAT},
  {801, "rotors/tail/rpm", simgear::props::FLOAT},
  {810, "rotors/main/blade[0]/position-deg",  simgear::props::FLOAT},
  {811, "rotors/main/blade[1]/position-deg",  simgear::props::FLOAT},
  {812, "rotors/main/blade[2]/position-deg",  simgear::props::FLOAT},
  {813, "rotors/main/blade[3]/position-deg",  simgear::props::FLOAT},
  {820, "rotors/main/blade[0]/flap-deg",  simgear::props::FLOAT},
  {821, "rotors/main/blade[1]/flap-deg",  simgear::props::FLOAT},
  {822, "rotors/main/blade[2]/flap-deg",  simgear::props::FLOAT},
  {823, "rotors/main/blade[3]/flap-deg",  simgear::props::FLOAT},
  {830, "rotors/tail/blade[0]/position-deg",  simgear::props::FLOAT},
  {831, "rotors/tail/blade[1]/position-deg",  simgear::props::FLOAT},

  {900, "sim/hitches/aerotow/tow/length",                       simgear::props::FLOAT},
  {901, "sim/hitches/aerotow/tow/elastic-constant",             simgear::props::FLOAT},
  {902, "sim/hitches/aerotow/tow/weight-per-m-kg-m",            simgear::props::FLOAT},
  {903, "sim/hitches/aerotow/tow/dist",                         simgear::props::FLOAT},
  {904, "sim/hitches/aerotow/tow/connected-to-property-node",   simgear::props::BOOL},
  {905, "sim/hitches/aerotow/tow/connected-to-ai-or-mp-callsign",   simgear::props::STRING},
  {906, "sim/hitches/aerotow/tow/brake-force",                  simgear::props::FLOAT},
  {907, "sim/hitches/aerotow/tow/end-force-x",                  simgear::props::FLOAT},
  {908, "sim/hitches/aerotow/tow/end-force-y",                  simgear::props::FLOAT},
  {909, "sim/hitches/aerotow/tow/end-force-z",                  simgear::props::FLOAT},
  {930, "sim/hitches/aerotow/is-slave",                         simgear::props::BOOL},
  {931, "sim/hitches/aerotow/speed-in-tow-direction",           simgear::props::FLOAT},
  {932, "sim/hitches/aerotow/open",                             simgear::props::BOOL},
  {933, "sim/hitches/aerotow/local-pos-x",                      simgear::props::FLOAT},
  {934, "sim/hitches/aerotow/local-pos-y",                      simgear::props::FLOAT},
  {935, "sim/hitches/aerotow/local-pos-z",                      simgear::props::FLOAT},

  {1001, "controls/flight/slats",  simgear::props::FLOAT},
  {1002, "controls/flight/speedbrake",  simgear::props::FLOAT},
  {1003, "controls/flight/spoilers",  simgear::props::FLOAT},
  {1004, "controls/gear/gear-down",  simgear::props::FLOAT},
  {1005, "controls/lighting/nav-lights",  simgear::props::FLOAT},
  {1006, "controls/armament/station[0]/jettison-all",  simgear::props::BOOL},

  {1100, "sim/model/variant", simgear::props::INT},
  {1101, "sim/model/livery/file", simgear::props::STRING},

  {1200, "environment/wildfire/data", simgear::props::STRING},
  {1201, "environment/contrail", simgear::props::INT},

  {1300, "tanker", simgear::props::INT},

  {1400, "scenery/events", simgear::props::STRING},

  {1500, "instrumentation/transponder/transmitted-id", simgear::props::INT},
  {1501, "instrumentation/transponder/altitude", simgear::props::INT},
  {1502, "instrumentation/transponder/ident", simgear::props::BOOL},
  {1503, "instrumentation/transponder/inputs/mode", simgear::props::INT},

  {10001, "sim/multiplay/transmission-freq-hz",  simgear::props::STRING},
  {10002, "sim/multiplay/chat",  simgear::props::STRING},

  {10100, "sim/multiplay/generic/string[0]", simgear::props::STRING},
  {10101, "sim/multiplay/generic/string[1]", simgear::props::STRING},
  {10102, "sim/multiplay/generic/string[2]", simgear::props::STRING},
  {10103, "sim/multiplay/generic/string[3]", simgear::props::STRING},
  {10104, "sim/multiplay/generic/string[4]", simgear::props::STRING},
  {10105, "sim/multiplay/generic/string[5]", simgear::props::STRING},
  {10106, "sim/multiplay/generic/string[6]", simgear::props::STRING},
  {10107, "sim/multiplay/generic/string[7]", simgear::props::STRING},
  {10108, "sim/multiplay/generic/string[8]", simgear::props::STRING},
  {10109, "sim/multiplay/generic/string[9]", simgear::props::STRING},
  {10110, "sim/multiplay/generic/string[10]", simgear::props::STRING},
  {10111, "sim/multiplay/generic/string[11]", simgear::props::STRING},
  {10112, "sim/multiplay/generic/string[12]", simgear::props::STRING},
  {10113, "sim/multiplay/generic/string[13]", simgear::props::STRING},
  {10114, "sim/multiplay/generic/string[14]", simgear::props::STRING},
  {10115, "sim/multiplay/generic/string[15]", simgear::props::STRING},
  {10116, "sim/multiplay/generic/string[16]", simgear::props::STRING},
  {10117, "sim/multiplay/generic/string[17]", simgear::props::STRING},
  {10118, "sim/multiplay/generic/string[18]", simgear::props::STRING},
  {10119, "sim/multiplay/generic/string[19]", simgear::props::STRING},

  {10200, "sim/multiplay/generic/float[0]", simgear::props::FLOAT},
  {10201, "sim/multiplay/generic/float[1]", simgear::props::FLOAT},
  {10202, "sim/multiplay/generic/float[2]", simgear::props::FLOAT},
  {10203, "sim/multiplay/generic/float[3]", simgear::props::FLOAT},
  {10204, "sim/multiplay/generic/float[4]", simgear::props::FLOAT},
  {10205, "sim/multiplay/generic/float[5]", simgear::props::FLOAT},
  {10206, "sim/multiplay/generic/float[6]", simgear::props::FLOAT},
  {10207, "sim/multiplay/generic/float[7]", simgear::props::FLOAT},
  {10208, "sim/multiplay/generic/float[8]", simgear::props::FLOAT},
  {10209, "sim/multiplay/generic/float[9]", simgear::props::FLOAT},
  {10210, "sim/multiplay/generic/float[10]", simgear::props::FLOAT},
  {10211, "sim/multiplay/generic/float[11]", simgear::props::FLOAT},
  {10212, "sim/multiplay/generic/float[12]", simgear::props::FLOAT},
  {10213, "sim/multiplay/generic/float[13]", simgear::props::FLOAT},
  {10214, "sim/multiplay/generic/float[14]", simgear::props::FLOAT},
  {10215, "sim/multiplay/generic/float[15]", simgear::props::FLOAT},
  {10216, "sim/multiplay/generic/float[16]", simgear::props::FLOAT},
  {10217, "sim/multiplay/generic/float[17]", simgear::props::FLOAT},
  {10218, "sim/multiplay/generic/float[18]", simgear::props::FLOAT},
  {10219, "sim/multiplay/generic/float[19]", simgear::props::FLOAT},

  {10300, "sim/multiplay/generic/int[0]", simgear::props::INT},
  {10301, "sim/multiplay/generic/int[1]", simgear::props::INT},
  {10302, "sim/multiplay/generic/int[2]", simgear::props::INT},
  {10303, "sim/multiplay/generic/int[3]", simgear::props::INT},
  {10304, "sim/multiplay/generic/int[4]", simgear::props::INT},
  {10305, "sim/multiplay/generic/int[5]", simgear::props::INT},
  {10306, "sim/multiplay/generic/int[6]", simgear::props::INT},
  {10307, "sim/multiplay/generic/int[7]", simgear::props::INT},
  {10308, "sim/multiplay/generic/int[8]", simgear::props::INT},
  {10309, "sim/multiplay/generic/int[9]", simgear::props::INT},
  {10310, "sim/multiplay/generic/int[10]", simgear::props::INT},
  {10311, "sim/multiplay/generic/int[11]", simgear::props::INT},
  {10312, "sim/multiplay/generic/int[12]", simgear::props::INT},
  {10313, "sim/multiplay/generic/int[13]", simgear::props::INT},
  {10314, "sim/multiplay/generic/int[14]", simgear::props::INT},
  {10315, "sim/multiplay/generic/int[15]", simgear::props::INT},
  {10316, "sim/multiplay/generic/int[16]", simgear::props::INT},
  {10317, "sim/multiplay/generic/int[17]", simgear::props::INT},
  {10318, "sim/multiplay/generic/int[18]", simgear::props::INT},
  {10319, "sim/multiplay/generic/int[19]", simgear::props::INT}
};

const unsigned int numProperties = (sizeof(sIdPropertyList)
                                 / sizeof(sIdPropertyList[0]));

// Look up a property ID using binary search.
namespace
{
  struct ComparePropertyId
  {
    bool operator()(const IdPropertyList& lhs,
                    const IdPropertyList& rhs)
    {
      return lhs.id < rhs.id;
    }
    bool operator()(const IdPropertyList& lhs,
                    unsigned id)
    {
      return lhs.id < id;
    }
    bool operator()(unsigned id,
                    const IdPropertyList& rhs)
    {
      return id < rhs.id;
    }
  };    
}

const IdPropertyList* findProperty(unsigned id)
{
  std::pair<const IdPropertyList*, const IdPropertyList*> result
    = std::equal_range(sIdPropertyList, sIdPropertyList + numProperties, id,
                       ComparePropertyId());
  if (result.first == result.second) {
    return 0;
  } else {
    return result.first;
  }
}

////////////////////////////////////////////////////////////////////////////
///// PROPERTIES TRANSMITTED
struct FGPropertyData {
  unsigned id;
  
  // While the type isn't transmitted, it is needed for the destructor
  simgear::props::Type type;
  union { 
    int int_value;
    float float_value;
    char* string_value;
  }; 
  
  ~FGPropertyData() {
    if ((type == simgear::props::STRING) || (type == simgear::props::UNSPECIFIED))
    {
      delete [] string_value;
    }
  }
};

typedef struct FGPropertyData *PFGPropertyData;

typedef std::vector<PFGPropertyData> pvProps;

static char *get_model_only( char *model )
{
    static char tmp_mod[256];
    int c;
    size_t i, len;
    char *cp = tmp_mod;
    char *md = cp;
    strcpy(cp,model);
    len = strlen(cp);
    for (i = 0; i < len; i++) {
        c = cp[i];
        if ((c == '\\') || (c == '/')) {
            md = &cp[i+1];
        }
    }
    return md;
}

static void Show_Pilots()
{
    size_t ii, max2, max = vPilots.size();
    PCF_Pilot pp;
    pvProps *pvp;
    SPRTF("%s: Have %d pilots in vector...\n", module, (int)max );
    for (ii = 0; ii < max; ii++) {
         pp = &vPilots[ii]; // get this pilots
         pvp = (pvProps *)pp->pvprops;
         max2 = 0;
         if (pvp) {
             max2 = pvp->size();
             delete pvp;
         }
         SPRTF("%10s %s pkts %d, props %d, unchanged %d,%d,%d\n", pp->callsign, pp->aircraft,
             pp->pkt_count, (int)max2,
             pp->unchanged_int_cnt, pp->unchanged_flt_cnt, pp->unchanged_stg_cnt);

    }
    vPilots.clear();

}


#ifdef USE_SIMGEAR  // TOCHECK SETPREVPOS MACRO
#define SETPREVPOS(p1,p2) { p1->ppx = p2->px; p1->ppy = p2->py; p1->ppz = p2->pz; }
#else // !USE_SIMGEAR
#define SETPREVPOS(p1,p2) { p1->PrevPos.Set( p2->SenderPosition.GetX(), p2->SenderPosition.GetY(), p2->SenderPosition.GetZ() ); }
#endif // USE_SIMGEAR y/n

#define SAME_FLIGHT(pp1,pp2)  ((strcmp(pp2->callsign, pp1->callsign) == 0) && (strcmp(pp2->aircraft, pp1->aircraft) == 0))

Packet_Type show_packet( PKTS &pkt )
{
    static CF_Pilot _s_new_pilot;
    char *packet = pkt.bgn;
    int len      = pkt.cnt; 
    uint32_t        MsgId;
    uint32_t        MsgMagic;
    uint32_t        MsgLen;
    uint32_t        MsgProto;
    T_PositionMsg*  PosMsg;
    PT_MsgHdr       MsgHdr;
    PCF_Pilot       pp2, pp = &_s_new_pilot;
    double          lat, lon, alt;
    double          px, py, pz;
    double          ox, oy, oz;
    time_t          curr_time = time(0);
    bool            is_upd = false;
    Packet_Type     pt = pkt_First; 

    memset(pp,0,sizeof(CF_Pilot)); // ensure new is ALL zero

    SPRTF("Len %u... ", pkt.cnt );
    MsgHdr    = (PT_MsgHdr)packet;
    MsgMagic  = XDR_decode<uint32_t> (MsgHdr->Magic);
    MsgId     = XDR_decode<uint32_t> (MsgHdr->MsgId);
    MsgLen    = XDR_decode<uint32_t> (MsgHdr->MsgLen);
    MsgProto  = XDR_decode<uint32_t> (MsgHdr->Version);
    if ((len < (int)MsgLen) || !((MsgMagic == RELAY_MAGIC)||(MsgMagic == MSG_MAGIC))||(MsgProto != PROTO_VER)) {
        if (len < (int)MsgLen) {
            SPRTF("Invalid MsgLen len %d\n", (int)MsgLen);
            return pkt_InvLen1;
        } else if ( !((MsgMagic == RELAY_MAGIC)||(MsgMagic == MSG_MAGIC)) ) {
            SPRTF("Invalid Magic\n");
            return pkt_InvMag;
        } else if ( !(MsgProto == PROTO_VER) ) {
            SPRTF("Invalid Proto\n");
            return pkt_InvProto;
        }
        SPRTF("Invalid!\n");
        return pkt_Invalid;
    }
    pp->curr_time = curr_time; // set CURRENT time
    pp->last_seen = curr_time;  // and LAST SEEN time
    if (MsgId == POS_DATA_ID)
    {
        SPRTF("POS ");
        if (MsgLen < sizeof(T_MsgHdr) + sizeof(T_PositionMsg)) {
            SPRTF("Invalid POS size\n");
            return pkt_InvLen2;
        }
        PosMsg = (T_PositionMsg *) (packet + sizeof(T_MsgHdr));
        pp->prev_time = pp->curr_time;
        pp->sim_time = XDR_decode64<double> (PosMsg->time); // get SIM time
        // get Sender address and port - need patch in fgms to pass this
        pp->SenderAddress = XDR_decode<uint32_t> (MsgHdr->ReplyAddress);
        pp->SenderPort    = XDR_decode<uint32_t> (MsgHdr->ReplyPort);
        px = XDR_decode64<double> (PosMsg->position[X]);
        py = XDR_decode64<double> (PosMsg->position[Y]);
        pz = XDR_decode64<double> (PosMsg->position[Z]);
        ox = XDR_decode<float> (PosMsg->orientation[X]);
        oy = XDR_decode<float> (PosMsg->orientation[Y]);
        oz = XDR_decode<float> (PosMsg->orientation[Z]);
        if ( (px == 0.0) || (py == 0.0) || (pz == 0.0)) {   
            SPRTF("Invalid pos\n");
            return pkt_InvPos;
        }
#ifdef USE_SIMGEAR // TOCHECK - Use SG functions

        SGVec3d position(px,py,pz);
        SGGeod GeodPoint;
        SGGeodesy::SGCartToGeod ( position, GeodPoint );
        lat = GeodPoint.getLatitudeDeg();
        lon = GeodPoint.getLongitudeDeg();
        alt = GeodPoint.getElevationFt();
        pp->px = px;
        pp->py = py;
        pp->pz = pz;
        SGVec3f angleAxis(ox,oy,oz);
        SGQuatf ecOrient = SGQuatf::fromAngleAxis(angleAxis);
        SGQuatf qEc2Hl = SGQuatf::fromLonLatRad((float)GeodPoint.getLongitudeRad(),
                                          (float)GeodPoint.getLatitudeRad());
        // The orientation wrt the horizontal local frame
        SGQuatf hlOr = conj(qEc2Hl)*ecOrient;
        float hDeg, pDeg, rDeg;
        hlOr.getEulerDeg(hDeg, pDeg, rDeg);
        pp->heading = hDeg;
        pp->pitch   = pDeg;
        pp->roll    = rDeg;
#else // #ifdef USE_SIMGEAR
        pp->SenderPosition.Set (px, py, pz);
        sgCartToGeod ( pp->SenderPosition, pp->GeodPoint );
        lat = pp->GeodPoint.GetX();
        lon = pp->GeodPoint.GetY();
        alt = pp->GeodPoint.GetZ();
#endif // #ifdef USE_SIMGEAR y/n
        pp->lat = lat;;
        pp->lon = lon;
        pp->alt = alt;
        if (alt <= -9990.0) {
            return pkt_InvHgt;
        }

        strcpy(pp->callsign,get_CallSign(MsgHdr->Callsign));
        if (m_Modify_AIRCRAFT)
            strcpy(pp->aircraft,get_Model(PosMsg->Model));
        else
            strcpy(pp->aircraft,PosMsg->Model);

        if (pp->callsign[0] == 0) {
            SPRTF("No callsign\n");
            return pkt_InvStg1;
        } else if (pp->aircraft[0] == 0) {
            SPRTF("No aircraft\n");
            return pkt_InvStg2;
        }
#ifdef USE_SIMGEAR  // TOCHECK SG function to get speed
        SGVec3f linearVel;
        for (unsigned i = 0; i < 3; ++i)
            linearVel(i) = XDR_decode<float> (PosMsg->linearVel[i]);
        pp->speed = norm(linearVel) * SG_METER_TO_NM * 3600.0;
#else // !#ifdef USE_SIMGEAR
        pp->SenderOrientation.Set ( ox, oy, oz );

        euler_get( lat, lon, ox, oy, oz,
            &pp->heading, &pp->pitch, &pp->roll );

        pp->linearVel.Set (
          XDR_decode<float> (PosMsg->linearVel[X]),
          XDR_decode<float> (PosMsg->linearVel[Y]),
          XDR_decode<float> (PosMsg->linearVel[Z])
            );
        pp->angularVel.Set (
          XDR_decode<float> (PosMsg->angularVel[X]),
          XDR_decode<float> (PosMsg->angularVel[Y]),
          XDR_decode<float> (PosMsg->angularVel[Z])
            );
        pp->linearAccel.Set (
          XDR_decode<float> (PosMsg->linearAccel[X]),
          XDR_decode<float> (PosMsg->linearAccel[Y]),
          XDR_decode<float> (PosMsg->linearAccel[Z])
            );
        pp->angularAccel.Set (
          XDR_decode<float> (PosMsg->angularAccel[X]),
          XDR_decode<float> (PosMsg->angularAccel[Y]),
          XDR_decode<float> (PosMsg->angularAccel[Z])
            );
        pp->speed = cf_norm(pp->linearVel) * SG_METER_TO_NM * 3600.0;
#endif // #ifdef USE_SIMGEAR
        SPRTF("cs=%s ", pp->callsign);
        SPRTF("ac=%s ", pp->aircraft);
        SPRTF("lat=%lf ", pp->lat);
        SPRTF("lon=%lf ", pp->lon);
        SPRTF("alt=%d ", (int)(pp->alt + 0.5));
        SPRTF("speed=%d ", (int)(pp->speed + 0.5));
        SPRTF("\n");
        pp->expired = false;
        size_t ii, max = vPilots.size();
        for (ii = 0; ii < max; ii++) {
            pp2 = &vPilots[ii]; // search list for this pilots
            if (SAME_FLIGHT(pp,pp2)) {
                is_upd = true;
                pt = pkt_Pos;
                pp2->last_seen = curr_time; // ALWAYS update 'last_seen'
                //seconds = curr_time - pp2->curr_time; // seconds since last PACKET
                //sseconds = pp->sim_time - pp2->sim_time; // curr packet sim time minus last packet sim time
                //revived = false;
#ifdef USE_SIMGEAR  // TOCHECK - SG to get diatance
                SGVec3d p1(pp->px,pp->py,pp->pz);       // current position
                SGVec3d p2(pp2->px,pp2->py,pp2->pz);    // previous position
                pp->dist_m = length(p2 - p1); // * SG_METER_TO_NM;
#else // !#ifdef USE_SIMGEAR
                pp->dist_m = (Distance ( pp2->SenderPosition, pp->SenderPosition ) * SG_NM_TO_METER); /** Nautical Miles to Meters */
#endif // #ifdef USE_SIMGEAR y/n
                pp->pvprops        = pp2->pvprops;  // use existing vector
                pp->flight_id      = pp2->flight_id; // use existing FID
                pp->first_sim_time = pp2->first_sim_time; // keep first sim time
                pp->first_time     = pp2->first_time; // keep first epoch time
                pp->expired          = false;
                pp->packetCount      = pp2->packetCount + 1;
                pp->packetsDiscarded = pp2->packetsDiscarded;
                pp->prev_sim_time    = pp2->sim_time;
                pp->prev_time        = pp2->curr_time;
                // accumulate total distance travelled (in nm)
                pp->total_nm         = pp2->total_nm + (pp->dist_m * SG_METER_TO_NM);
                SETPREVPOS(pp,pp2);  // copy POS to PrevPos to get distance travelled
                pp->curr_time        = curr_time; // set CURRENT packet time
                *pp2 = *pp;     // UPDATE the RECORD with latest info
                pp = pp2;
                break;
            }
        }
        pp->pkt_count++;
        if (!is_upd) {
            pp->pvprops = new pvProps;
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        if (add_properies) {
            xdr_data_t *xdr = reinterpret_cast<xdr_data_t*>(packet + sizeof(T_MsgHdr) + sizeof(T_PositionMsg));
            xdr_data_t *msgEnd = reinterpret_cast<xdr_data_t*>(packet + len);
            uint32_t val = 0;
            float fval = 0.0;
            uint32_t length = 0;
            uint32_t i;
            char *string_value = 0;
            uint32_t id = 0;
            char c;
            int prop_count = 0;
            while ( xdr < msgEnd ) {
                prop_count++;
                //unsigned id = XDR_decode_uint32(*xdr);
                id = XDR_decode<uint32_t> (*xdr);
                //cout << pData->id << " ";
                xdr++;
                // Check the ID actually exists and get the type
                const IdPropertyList* plist = findProperty(id);
                FGPropertyData* pData = 0;
                if (plist) {
                    pData = new (std::nothrow) FGPropertyData;
                }
                if (plist && pData) {
                    pData->id = id;
                    pData->type = plist->type;

                    // How we decode the remainder of the property depends on the type
                    //switch (pData->type) {
                    switch (plist->type) {
                    case simgear::props::INT:
                    case simgear::props::BOOL:
                    case simgear::props::LONG:
                        val = XDR_decode<uint32_t> (*xdr);
                        //  pData->int_value = XDR_decode_uint32(*xdr);
                        pData->int_value = val;
                        xdr++;
                        if (VERB5) {
                            SPRTF("INT: %s val %ld\n", plist->name, val );
                        }
                        break;
                    case simgear::props::FLOAT:
                    case simgear::props::DOUBLE:
                        //pData->float_value = XDR_decode_float(*xdr);
                        fval = XDR_decode<float>(*xdr);
                        xdr++;
                        pData->float_value = fval;
                        //cout << pData->float_value << "\n";
                        if (VERB5) {
                            SPRTF("FLT: %s val %ld\n", plist->name, val );
                        }
                        break;
                    case simgear::props::STRING:
                    case simgear::props::UNSPECIFIED:
                        {
                            // String is complicated. It consists of
                            // The length of the string
                            // The string itself
                            // Padding to the nearest 4-bytes.    
                            length = XDR_decode<uint32_t> (*xdr); // XDR_decode_uint32(*xdr);
                            xdr++;
                            //cout << length << " ";
                            // Old versions truncated the string but left the length unadjusted.
                            if (length > MAX_TEXT_SIZE)
                                length = MAX_TEXT_SIZE;

                            string_value = new char[length + 2];
                            if (length) {
                                //pData->string_value = new char[length + 1];
                                //cout << " String: ";
                                for (i = 0; ((i < length) && (xdr < msgEnd)); i++) {
                                    //pData->string_value[i] = (char) XDR_decode_int8(*xdr);
                                    c = (char) XDR_decode<char>(*xdr);
                                    xdr++;
                                    if (c < ' ')
                                        c = '.';
                                    if (c > 127)
                                        c = '.';
                                    string_value[i] = c;
                                    //cout << pData->string_value[i];
                                }

                                // pData->string_value[length] = '\0';
                                string_value[i] = '\0';

                                if (VERB5) {
                                    SPRTF("STG: %s val %s\n", plist->name, string_value );
                                }
                            } else {
                                string_value[0] = '\0';
                                if (VERB5) {
                                    SPRTF("STG: %s with no val\n", plist->name );
                                }
                            }
                            pData->string_value = string_value;
                            // Now handle the padding
                            while (((length % 4) != 0) && (xdr < msgEnd)) {
                                xdr++;
                                length++;
                                //cout << "\n";
                            }
                        }
                        break;
                    default:
                        SPRTF("Unknown Prop type %d %d\n", (int)id, (int)plist->type);
                        xdr++;
                        break;
                    }
                } else {
                    if (add_to_vector( id )) {
                        SPRTF("Property ID %d NOT FOUND\n", (int)id );
                    }
                }
                if (pData) {
                    PFGPropertyData pd;
                    bool found = false;
                    pvProps *pvp = (pvProps *)pp->pvprops;
                    max = pvp->size();
                    for (ii = 0; ii < max; ii++) {
                        pd = pvp->at(ii);
                        if (pd->id == pData->id) {
                            found = true;
                            switch (pd->type) {
                            case simgear::props::INT:
                            case simgear::props::BOOL:
                            case simgear::props::LONG:
                                if (pd->int_value == pData->int_value) {
                                    unchanged_int_cnt++;
                                    pp->unchanged_int_cnt++;
                                } else {
                                    pd->int_value = pData->int_value;
                                }
                                break;
                            case simgear::props::FLOAT:
                            case simgear::props::DOUBLE:
                                if (pd->float_value == pData->float_value) {
                                    unchanged_flt_cnt++;
                                    pp->unchanged_flt_cnt++;
                                } else {
                                    pd->float_value = pData->float_value;
                                }
                                break;
                            case simgear::props::STRING:
                            case simgear::props::UNSPECIFIED:
                                if (strcmp(pd->string_value,pData->string_value)) {
                                    //delete pd->string_value;
                                    pd->string_value = string_value;
                                } else {
                                    pp->unchanged_stg_cnt++;
                                    unchanged_stg_cnt++;
                                }
                                break;
                            }

                            break;
                        }
                    }
                    if (found) {
                        delete pData;
                    } else {
                        pvp->push_back(pData);
                    }
                }
            }
            if (prop_count > pp->max_props) {
                pp->max_props = prop_count;
            }
        }
        if (!is_upd) {
            vPilots.push_back(*pp);
        }
        return pt;
    } else if (MsgId == CHAT_MSG_ID) {
        SPRTF("Chat msg\n");
        return pkt_Chat;
    }
    SPRTF("OTHER msg\n");
    return pkt_Other;
}

// packet sizes, in 100s 0f byte
static int pkt_sizes[20] = {0};
void Show_Pkt_Counts()
{
    int i, cnt, total = 0;
    double pct;
    for (i = 0; i < 20; i++) {
        total += pkt_sizes[i];
    }
    SPRTF("%s: Total packets %d\n", module, total );
    for (i = 0; i < 20; i++) {
        cnt = pkt_sizes[i];
        if (cnt) {
            pct = ((double)cnt / (double)total) * 100.0;
            SPRTF("Size %d to %d %lf percent...\n", (i * 100), ((i + 1) * 100), pct);
        }
    }
}

int load_cf_log()
{
    struct stat buf;
    const char *tf = usr_input;
    int file_size, file_read, len;
    if (stat(tf,&buf)) {
        SPRTF("stat of %s file failed!\n",tf);
        return 1;
    }
    file_size = (int)buf.st_size;
	SPRTF("%s: Creating %d byte buffer...\n", module, MAX_BUFFER_SIZE+2 );
    char *tb = (char *)malloc( MAX_BUFFER_SIZE+2 );
    if (!tb) {
        SPRTF("malloc(%d) file failed!\n",(int) MAX_BUFFER_SIZE+2);
        return 2;
    }
    FILE *fp = fopen(tf,"rb");
    if (!fp) {
        SPRTF("open of %s file failed!\n",tf);
        free(tb);
        return 3;
    }

	SPRTF("%s: Processing file %d bytes, buffer by buffer...\n", mod_name, file_size );
    file_read = 0;
    int i, c;
    char *pbgn = 0;
    int packets = 0;
    int cnt = 0;
    int off = 0;
    int cnt2;
    Packet_Type pt;
    PPKTSTR pps = &sPktStr[0];
    Clear_Packet_Stats();
    //time_t curr, last_json, last_stat;
    //PKT pkt;
    //Packet_Type pt;
    //struct timespec req;
    PKTS pkt;
    //PPKTSTR ppt = Get_Pkt_Str();
    //clear_prop_stats();
    //clear_upd_type_stats();
    //curr = last_json = last_stat = 0;
    packets = 0;
    while ( file_read < file_size ) {
        len = fread(tb+off,1,MAX_BUFFER_SIZE-off,fp);
        if (len <= 0)
            break;
        file_read += len;
        len += off; // add remainder from last read
        for (i = 0; i < len; i++) {
            c = tb[i];
            // 53 46 47 46 00 01 00 01 00 00 00 07
            //if ((c == 'S') && (tb[i+1] == 'F') && (tb[i+2] == 'G') && (tb[i+3] == 'F')) 
            if ((tb[i+0] == 0x53)&&(tb[i+1] == 0x46)&&(tb[i+2] == 0x47)&&(tb[i+3] == 0x46)&&
                (tb[i+4] == 0x00)&&(tb[i+5] == 0x01)&&(tb[i+6] == 0x00)&&(tb[i+7] == 0x01)&&
                (tb[i+8] == 0x00)&&(tb[i+9] == 0x00)&&(tb[i+10]== 0x00)&&(tb[i+11]== 0x07)) {
                if (pbgn && cnt) {
                    packets++;
                    // if (M_VERB9) SPRTF("Deal with packet len %d\n", cnt);
                    pkt.bgn = pbgn;
                    pkt.cnt = cnt;
                    pt = show_packet(pkt);
                    if (pt < pkt_Max) sPktStr[pt].count++;  // set the packet stats
                    off = len - cnt;
                    cnt2 = cnt / 100;
                    pkt_sizes[cnt2]++;
                    break;  // to read and fill buffer
                }
                pbgn = &tb[i];
                cnt = 0;
            }
            cnt++;
        }
        c = 0;
        for ( ; i < len; i++) {
            tb[c++] = tb[i];    // move remaining data to head of buffer
        }
        pbgn = 0;
        cnt = 0;
    }
    fclose(fp);
    if (pbgn && cnt) {
       packets++;
    }
    SPRTF("%s: Dealt with %d packets...\n", module, packets);
    Show_Pkt_Counts();
    Show_Pilots();
    return 0;
}


// log sample : C:\Users\user\Downloads\logs\fgx-cf\cf_raw.log
// /home/geoff/downloads/cf_raw.log
// main() OS entry
int main( int argc, char **argv )
{
    int iret = 0;
    set_log_file((char *)def_log,false);
    iret = parse_args(argc,argv);
    if (iret)
        return iret;

    iret = load_cf_log(); // actions of app

    return iret;
}


// eof = cf_log.cxx
