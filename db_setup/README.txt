file : README.sql

The tables MUST be added to the database(s) before 
running the tracker code... postgresql or sqlite3

To aid in this two scripts are given.

create_pg.sql

This has been tested and works for PostgreSQL


create_db.sql

A more generic SQL script that should also function 
with SQlite3

;=============================
; other setup stuff

##### With 9.5

Created user(role) fgtracker, pwd fgtracker, with ability to create a database, then

F:\FG\18\crossfeed\db_setup>psql -U fgtracker -p 5433 tracker_test
Password for user fgtracker:
psql (9.5beta2)
WARNING: Console code page (437) differs from Windows code page (1252)
         8-bit characters might not work correctly. See psql reference
         page "Notes for Windows users" for details.
Type "help" for help.

tracker_test=# \i 'create_db.sql'
psql:create_db.sql:13: NOTICE:  index "ix_flights_model" does not exist, skipping
DROP INDEX
psql:create_db.sql:14: NOTICE:  index "ix_flights_status" does not exist, skipping
DROP INDEX
psql:create_db.sql:15: NOTICE:  index "ix_flights_fid" does not exist, skipping
DROP INDEX
psql:create_db.sql:16: NOTICE:  index "ix_flights_callsign" does not exist, skipping
DROP INDEX
psql:create_db.sql:17: NOTICE:  index "ix_positions_fid" does not exist, skipping
DROP INDEX
psql:create_db.sql:18: NOTICE:  index "ix_positions_ts" does not exist, skipping
DROP INDEX
psql:create_db.sql:25: NOTICE:  table "flights" does not exist, skipping
DROP TABLE
psql:create_db.sql:26: NOTICE:  table "positions" does not exist, skipping
DROP TABLE
CREATE TABLE
CREATE INDEX
CREATE INDEX
CREATE INDEX
CREATE INDEX
CREATE TABLE
CREATE INDEX
CREATE INDEX
CREATE VIEW
CREATE VIEW
tracker_test=# \q
Release\test_pg.exe -p 5433 -d tracker_test -u fgtracker -w fgtracker

##### With 9.1.7
1: Add database $ createdb -U postgres tracker_test
2: createuser -U postgres
Enter name of role to add: cf
Shall the new role be a superuser? (y/n) n
Shall the new role be allowed to create databases? (y/n) y
Shall the new role be allowed to create more new roles? (y/n) n
Password: [postgres]
3: psql -U postgres
4: Create tables in traacker_test db
$ psql tracker_test cf
Password for user cf:
psql (9.1.7)
tracker_test=$ \i 'create_db.sql'
tracker_test=$ \q


# eof
