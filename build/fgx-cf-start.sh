#!/bin/sh
#< fgx-cf-start.sh - 20121106 - check if already running, if not start ...
# 20130103 - MUST be run as user 'crossfeed'
# 20121212 - MUST be run under user 'cf'
# RELEASED UNDER GNU GPLv3 (or later)
# Geoff R. McLane (reports (at) geoffair (dot) info) - 2012.
BN=`basename $0`

MAPP="cf_client"
TUSR="crossfeed"
THM="/home/cf"

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "Aborting ... no input!"
        else
            echo "Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}

RUNCMD="$MAPP"
#if [ -f "$MAPP" ]; then
#    RUNCMD="./$MAPP"
#fi

FORCE=0
CMDOPTS=""
for arg in $@; do
   if [ "$arg" = "-f" ]; then
       FORCE=1
   else
       CMDOPTS="$CMDOPTS $arg"
   fi
done

# if [ ! "$USER" = "$TUSR" ] || [ ! "$HOME" = "$THM" ]; then
if [ ! "$USER" = "$TUSR" ]; then
	echo "$BN: ERROR: This application MUST be run as user $TUSR!"
	echo "$BN: Use '$ sudo su -l $TUSR' to switch users."
	echo "$BN: Aborting..."
	exit 1
fi

# LUID=$(id -u)
# if [ "$LUID" -ne "0" ]; then
#    echo "$BN: $RUNCMD must be run as root, so will need password later..."
#    #exit 1
#    RUNCMD="sudo $RUNCMD"
# fi

$RUNCMD -? > /dev/null 2>&1
if [ "$?" -ne "0" ]; then
    echo "$BN: Unable to find, run '$MAPP'! Is it in your PATH?"
    exit 1
else
    echo "$BN: Good '$MAPP' found in PATH..."
fi

echo "$BN: Checking if '$MAPP' running already..."

#TEST=`ps -A | grep $MAPP | grep -v grep`
TEST=`ps -A -o user,pid,ucmd | grep $MAPP | grep -v grep | grep -v $BN`
FND=0
TPID=""
for arg in $TEST; do
    echo $arg | grep -o ^[0-9][0-9]*[0-9]$ > /dev/null
    if [ "$?" = "0" ]; then
        TPID="$arg"
    fi
    if [ "$arg" = "$MAPP" ]; then
        FND=1
        break
    fi
done
if [ "$FND" = "1" ]; then
    echo ""
    echo "$BN: Appears '$MAPP' to already be running... PID $TPID"
    # echo "$BN: Use 'fgx-cf-stop.sh' to stop it..."
    echo "$BN: Use 'sudo kill $TPID' to stop it"
    if [ "$FORCE" = "1" ]; then
        echo "$BN: Are you SURE you want to continue?"
        ask
    else
        # echo "$BN: Use -f to FORCE a second copy..."
        echo ""
        exit 1
    fi
else
    echo "$BN: No trace found of '$MAPP'..."
fi

echo "$BN: Starting '$MAPP'..."

echo "$BN: Doing '$RUNCMD -d $CMDOPTS'"
$RUNCMD -d $CMDOPTS

#TEST2=`ps -A | grep $MAPP | grep -v grep`
TEST2=`ps -A -o user,pid,ucmd | grep $MAPP | grep -v grep | grep -v $BN`
FND2=0
TPID=""
for arg in $TEST2; do
    echo $arg | grep -o ^[0-9][0-9]*[0-9]$ > /dev/null
    if [ "$?" = "0" ]; then
        TPID="$arg"
    fi
    if [ "$arg" = "$MAPP" ]; then
        FND2=1
        break
    fi
done
# echo "$TEST2"
if [ "$FND2" = "1" ]; then
    if [ "$FND" = "1" ]; then
        echo "$BN: Appears to '$MAPP' is running... PID $TPID, but it was before anyway!!!"
        echo "$BN: The second copy would have FAILED, if it was run with the SAME parameters..."
    else
        echo "$BN: Appears the '$MAPP' is now running... PID $TPID"
    fi
else
    echo "$BN: No trace found of '$MAPP'... Appears start FAILED!"
    exit 1
fi

exit 0

# eof
