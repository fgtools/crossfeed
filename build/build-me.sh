#!/bin/sh
#< build-fgx-cf.sh - 20150626 - 20130103 - 20121221 
# 20130411 - Change to using a 'build' sub-directory
BN=`basename $0`

# relative SOURCE - adjust as needed
TMPSRC=".."
TMPCM="$TMPSRC/CMakeLists.txt"
TMPLOG="bldlog-1.txt"

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "$BN: Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "$BN: Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "$BN: Aborting ... no input!"
        else
            echo "$BN: Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}


if [ ! -f "$TMPCM" ]; then
    echo "$BN: ERROR: Can NOT locate [$TMPCM] file! Check name, location, and FIX ME $0"
    exit 1
fi

CMOPTS=""
VERSBOSE=0
TMPRB="rem-bins.sh"
NOSG=1
SG_DIRECTORY=
SG_LIB="libSimGearCore.a"
ADD_SQLITE3=0
NO_GEOD_LIB=0
NO_PGSQL=0
ADD_MG_LIB=0
DO_CMAKE=0
ADD_DEBUG=0

if [ -f "$TMPLOG" ]; then
    rm -f $TMPLOG
fi

give_help()
{
    echo "$BN [OPTIONS]"
    echo "OPTIONS"
    echo " VERBOSE = Use verbose build"
    echo " CLEAN   = Clean and exit."
    echo " CLEANALL= Clean all, including binaries, and exit."
    echo " DOCMAKE = Do cmake configuration whether Makefile exists or not."
    echo " NOSG    = Exclude linking with SimGear core library."
    echo " ADDSG   = Add linking with SimGear core library."
    echo " SQLITE3 = Add in SQLite3 tracker database. Disables PostgreSQL"
    echo " NOPGSQL = Disable PostgreSQL"
    echo " NOGEOD  = Exclude GeographicLib from build, and the test_pg2 app"
    echo " SGDIR=/path/to/sg = Set to find SG includes and library"
    echo " ADDMG   = Add mongoose library build"
    echo " NOMG    = Disable mongoose library build (default)"
    echo " DEBUG   = Build Debug version (default is Release)"
    echo " Note: This path must be the root where /lib/$SG_LIB exists."
    echo ""
    exit 1
}

do_clean()
{
    echo "$BN: Doing 'cmake-clean'"
    cmake-clean
    echo "$BN: Done 'cmake-clean'"
    exit 0
}

do_clean_all()
{
    echo "$BN: Doing 'cmake-clean'"
    cmake-clean
    echo "$BN: Done 'cmake-clean'"
    if [ -f "$TMPRB" ]; then
        echo "$BN: Also doing: './$TMPRB'"
        ./$TMPRB
    #else
    #    echo "$BN: Unable to find $TMPRB, to remove binaries..."
    fi
    exit 0
}

#         SGDIR=*) set_sg_dir $arg ;;
set_sg_dir()
{
    echo "$BN: Setting SG directory path from [$1]"
    CNT=`expr length $1`
    SG_DIRECTORY=`echo $1 | cut -b7-$CNT`
    if [ ! -d "$SG_DIRECTORY" ]; then
        echo "$BN: ERROR: Directory [$SG_DIRECTORY] does NOT exist! Aborting..."
        exit 1
    fi
    TMP="$SG_DIRECTORY/$SG_LIB"
    if [ -f "$TMP" ]; then
        echo "$BN: This is the FULL library path. Only need root, minus /lib. Aborting..."
        exit 1;
    fi
    TMP="$SG_DIRECTORY/lib/$SG_LIB"
    if [ -f "$TMP" ]; then
        echo "$BN: Found [$TMP]. Will use this path"
        if [ -f "sg_path" ]; then
            rm -f sg_path
        fi
        echo "SG_DIRECTORY=$SG_DIRECTORY" > sg_path
    else
        TMP2="$SG_DIRECTORY/lib/x86_64-linux-gnu/$SG_LIB"
        if [ -f "$TMP2" ]; then
            echo "$BN: Found [$TMP2]. Will use this path"
            if [ -f "sg_path" ]; then
                rm -f sg_path
            fi
            echo "SG_DIRECTORY=$SG_DIRECTORY" > sg_path
        else
            echo "$BN: FAILED! Can NOT find [$TMP]! Aborting..."
            exit 1
        fi
    fi
    NOSG=0
    ADDSG=1
}

if [ -f sg_path ]; then
    . ./sg_path
fi

for arg in $@; do
      case $arg in
         VERBOSE) VERBOSE=1 ;;
         CLEAN) do_clean ;;
         CLEANALL) do_clean_all ;;
         DOCMAKE) DO_CMAKE=1 ;;
         NOSG) NOSG=1 ;;
         ADDSG) NOSG=0 ;;
         SGDIR=*) set_sg_dir $arg ;;
         SQLITE3) ADD_SQLITE3=1 ;;
         NOPGSQL) NO_PGSQL=1 ;;
         NOGEOD) NO_GEOD_LIB=1 ;;
         ADDMG) ADD_MG_LIB=1 ;;
         NOMG) ADD_MG_LIB=0 ;;
         DEBUG) ADD_DEBUG=1 ;;
         --help) give_help ;;
         -h) give_help ;;
         -\?) give_help ;;
         *)
            echo "$BN: ERROR: Invalid argument [$arg]"
            exit 1
            ;;
      esac
done

echo "$BN: Commence build of crossfeed..." > $TMPLOG

if [ "$ADD_DEBUG" = "1" ]; then
    CMOPTS="$CMOPTS -DCMAKE_BUILD_TYPE=Debug -DENABLE_DEBUG_SYMBOLS:BOOL=TRUE"
else
    CMOPTS="$CMOPTS -DCMAKE_BUILD_TYPE=Release"
fi

if [ "$VERBOSE" = "1" ]; then
    CMOPTS="$CMOPTS -DCMAKE_VERBOSE_MAKEFILE=TRUE"
    echo "$BN: Enabling VERBOSE make"
    echo "$BN: Enabling VERBOSE make" >> $TMPLOG
fi

if [ "$NOSG" = "1" ]; then
    echo "$BN: Building WITHOUT SG library"
    echo "$BN: Building WITHOUT SG library" >> $TMPLOG
    CMOPTS="$CMOPTS -DUSE_SIMGEAR_LIB:BOOL=FALSE"
else
    echo "$BN: Need SG library available"
    echo "$BN: Need SG library available" >> $TMPLOG
    CMOPTS="$CMOPTS -DUSE_SIMGEAR_LIB:BOOL=TRUE -DCMAKE_PREFIX_PATH=$SG_DIRECTORY"
    export BOOST_ROOT=/usr
    if [ -z "$SG_DIRECTORY" ]; then
        TMP="/usr/local/lib/$SG_LIB"
        if [ -f "$TMP" ]; then
            set_sg_dir "SGDIR=/usr/local"
        else
            echo "$BN: ERROR: No SG library directory available!"
            echo "$BN: Tried /usr/local... that is '/usr/local/lib/$SG_LIB'!"
            echo "$BN: Use option 'SGDIR=/path/to/sglibrary/root', or add option 'NOSG'"
            echo "$BN: Or create a file 'sg_path', with 'SG_DIRECTORY=/path/to/sg'"
            echo "$BN: Do not add '/lib', nor the library name to the path"
            echo "$BN: And re-run this scripts. Aborting..."
            exit 1
        fi
    fi
fi

if [ "$ADD_SQLITE3" = "1" ]; then
	echo "$BN: Adding SQLite3 tracker support, disable PostgreSQL"
	echo "$BN: Adding SQLite3 tracker support, disable PostgreSQL" >> $TMPLOG
	CMOPTS="$CMOPTS -DUSE_SQLITE3_DATABASE:BOOL=TRUE"
	CMOPTS="$CMOPTS -DUSE_POSTGRESQL_DATABASE:BOOL=FALSE"
else
	echo "$BN: No SQLite3 tracker support"
	echo "$BN: No SQLite3 tracker support" >> $TMPLOG
	if [ "$NO_PGSQL" = "1" ]; then
    	CMOPTS="$CMOPTS -DUSE_POSTGRESQL_DATABASE:BOOL=FALSE"
    	echo "$BN: Disable PostgreSQL"
    	echo "$BN: Disable PostgreSQL" >> $TMPLOG
    else
    	CMOPTS="$CMOPTS -DUSE_POSTGRESQL_DATABASE:BOOL=ON"
	fi
fi
if [ "$NO_GEOD_LIB" = "1" ]; then
    echo "$BN: Disabling GeographicLib link, thus no test_pg2 app"
    echo "$BN: Disabling GeographicLib link, thus no test_pg2 app" >> $TMPLOG
    CMOPTS="$CMOPTS -DUSE_GEOGRAPHIC_LIB:BOOL=FALSE"
else
    echo "$BN: Includes link with GeographicLib, and test_pg2 app."
    echo "$BN: Includes link with GeographicLib, and test_pg2 app." >> $TMPLOG
fi

if [ "$ADD_MG_LIB" = "1" ]; then
    echo "$BN: Enabling mongoose library build"
    echo "$BN: Enabling mongoose library build" >> $TMPLOG
    CMOPTS="$CMOPTS -DUSE_MONGOOSE_LIB:BOOL=TRUE"
else
    echo "$BN: Disabling mongoose library build"
    echo "$BN: Disabling mongoose library build" >> $TMPLOG
    CMOPTS="$CMOPTS -DUSE_MONGOOSE_LIB:BOOL=FALSE"
fi



if [ ! -f "Makefile" ] || [ "$DO_CMAKE" = "1" ]; then
    echo "$BN: Doing: 'cmake $CMOPTS $TMPSRC', output to $TMPLOG"
    echo "$BN: Doing: 'cmake $CMOPTS $TMPSRC'" >> $TMPLOG
    cmake $CMOPTS $TMPSRC >> $TMPLOG 2>&1
    if [ ! "$?" = "0" ]; then
        echo "$BN: cmake configuration, generation error, see $TMPLOG"
        exit 1
    fi
else
    echo "$BN: Found Makefile - run CLEAN or CLEANALL to run CMake again..."
    echo "$BN: Found Makefile - run CLEAN or CLEANALL to run CMake again..." >> $TMPLOG
fi

if [ ! -f "Makefile" ]; then
    echo "$BN: ERROR: 'cmake $TMPOPTS ..' FAILED to create 'Makefile', see $TMPLOG"
    echo "$BN: ERROR: 'cmake $TMPOPTS ..' FAILED to create 'Makefile'" >> $TMPLOG
    exit 1
fi

echo "$BN: Doing: 'make', output to $TMPLOG"
echo "$BN: Doing: 'make'" >> $TMPLOG
make >> $TMPLOG 2>&1
if [ ! "$?" = "0" ]; then
    echo "$BN: Appears to be a 'make' error... see $TMPLOG"
    exit 1
else
    echo "$BN: Appears to be successful make ;=))"
    echo "$BN: Appears to be successful make ;=))" >> $TMPLOG
fi

# eof

