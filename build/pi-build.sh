#!/bin/sh
#< pi-build.sh - pi build preferences
###########################################
### WARNING: Just my chosen preferences ###
### Adjust to suit YOUR environment     ###
###########################################
# TMPSG="/home/geoff/fg/next/install/simgear"
# TMPSG="/media/Disk2/FG/fg22/install/simgear"
TMPSG2="/media/pi/SAMSUNG2/projects/FG/next/install/simgear"

if [ ! -d "$TMPSG2" ]; then
    echo "Error: Oops, can not FIND $TMPSG2! *** FIX ME ***"
    exit 1
fi

############# make sure to FIX this Simgear PATH #######
BLDOPS="SGDIR=$TMPSG2 ADDMG DOCMAKE VERBOSE"
# BLDOPS="NOSG ADDMG DOCMAKE VERBOSE"
### Can NOT fid all PostgreSQL stuff, so
BLDOPS="$BLDOPS NOPGSQL"

./build-me.sh $BLDOPS $@

# eof
