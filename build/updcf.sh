#!/bin/sh
#< updcf.sh - 20121228 - 20121025
BN=`basename $0`

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "$BN: Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "$BN: Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "$BN: Aborting ... no input!"
        else
            echo "$BN: Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}

TMPREPO="git@gitlab.com:fgtools/crossfeed.git"
#TMPREPO="git@gitorious.org:fgtools/crossfeed.git"
TMPDIR="crossfeed"

if [ -d "$TMPDIR" ]; then

    echo "$BN: This is an update of $TMPDIR"
    cd $TMPDIR
    git status
    echo "$BN: Will do in $(pwd): 'git pull'"
    ask
    echo "$BN: Doing in $(pwd): 'git pull'"
    git pull
else
    echo "$BN: No $TMPDIR! This looks like a NEW clone..."
    echo "$BN: Will do: 'git clone $TMPREPO $TMPDIR'"
    ask
    echo "$BN: Doing: 'git clone $TMPREPO $TMPDIR'"
    git clone $TMPREPO $TMPDIR
    if [ ! -d "$TMPDIR" ]; then
        echo "$BN: Appears clone FAILED!"
        exit 1
    fi
    echo "$BN: clone completed..."
fi

# eof - updcf.sh

