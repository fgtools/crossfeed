#!/bin/sh
#< fgx-cf-start.sh - 20120103 - 20121106 - check if running...

# RELEASED UNDER GNU GPLv3 (or later)
# Geoff R. McLane (reports (at) geoffair (dot) info) - 2012.
BN=`basename $0`

MAPP="cf_client"

wait_for_input()
{
    if [ "$#" -gt "0" ] ; then
        echo "$1"
    fi
    echo -n "Enter y to continue : "
    read char
    if [ "$char" = "y" -o "$char" = "Y" ]
    then
        echo "Got $char ... continuing ..."
    else
        if [ "$char" = "" ] ; then
            echo "Aborting ... no input!"
        else
            echo "Aborting ... got $char!"
        fi
        exit 1
    fi
}

ask()
{
    wait_for_input "$BN: *** CONTINUE? ***"
}

echo ""

RUNCMD="$MAPP"
# LUID=$(id -u)
# if [ "$LUID" -ne "0" ]; then
#    echo "$BN: $RUNCMD must be run as root, so will need password later..."
#    #exit 1
#    RUNCMD="sudo $MAPP"
# fi
WHO=`whoami`
if [ ! "$WHO" = "crossfeed" ]; then
echo "$BN: Note cf_client should be run as 'crossfeed'"
fi

echo "$BN: Checking if '$MAPP' is running..."

TEST=`ps -A -o user,pid,ucmd | grep $MAPP | grep -v grep | grep -v $BN`
# TEST=`ps -A | grep $MAPP | grep -v grep`
FND=0
TPID=""
for arg in $TEST; do
    # echo "$arg"
    echo $arg | grep -o ^[0-9][0-9]*[0-9]$ > /dev/null
    if [ "$?" = "0" ]; then
        TPID="$arg"
    fi
    if [ "$arg" = "$MAPP" ]; then
        FND=1
        break
    fi
done
if [ "$FND" = "1" ]; then
    echo "$BN: Appears '$MAPP' is running... PID $TPID"
else
    echo "$BN: No trace if '$MAPP' found..."
fi
echo ""

# eof - fgx-cf-find.sh

