@setlocal

@set TMPRT=..
@set TMPVER=1
@set TMPPROJ=crossfeed
@set TMPPRJ=cf
@set TMPDRV=X:
@set DOINSTALL=0
@set DOTMPINST=0
@set SETUPPG=0
@set TMPBOOST=C:\local\boost_1_62_0
@set TMPSRC=%TMPRT%
@set TMPBGN=%TIME%
@set TMPINS=%TMPDRV%\3rdParty
@call chkmsvc %TMPPROJ% 
@set TMPLOG=bldlog-%TMPVER%.txt
@set TMPSG=%TMPDRV%\install\msvc140\SimGear
@if NOT EXIST %TMPSG%\nul goto NOSG

@set TMPOPTS=
@set TMPOPTS=%TMPOPTS% -DCMAKE_INSTALL_PREFIX=%TMPINS%
@set TMPOPTS=%TMPOPTS% -DCMAKE_PREFIX_PATH:PATH=%TMPSG%
@REM ====================================================================
@REM 20140919: Did an important FIX to the get euler alternate maths, so SimGear
@REM is no longer needed. Defaults to OFF in the CMakeLists.txt
@REM ====================================================================
@REM set TMPOPTS=%TMPOPTS% -DUSE_POSTGRESQL_DATABASE:BOOL=OFF
@set TMPOPTS=%TMPOPTS% -DUSE_SIMGEAR_LIB:BOOL=ON

@REM 20141007: Add mongoose library for http handling
@set TMPOPTS=%TMPOPTS% -DUSE_MONGOOSE_LIB:BOOL=TRUE

:RPT
@if "%~1x" == "x" goto GOTCMD
@set TMPOPTS=%TMPOPTS% %1
@shift
@goto RPT
:GOTCMD

@echo Build %DATE% %TIME% > %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG%
@echo Build source %TMPSRC%... all output to build log %TMPLOG% >> %TMPLOG%

@REM echo Establish MSVC + SDK environment >> %TMPLOG%
@REM call set-msvc-sdk >> %TMPLOG%

@if NOT EXIST %TMPBOOST%\nul goto NOBST
@set Boost_DIR=%TMPBOOST%
@set BOOST_ROOT=%TMPBOOST%
@echo Set ENV Boost_DIR=%Boost_DIR% BOOST_ROOT=%BOOST_ROOT% >> %TMPLOG%
@REM set TMPOPTS=%TMPOPTS% -DBoost_DEBUG:BOOL=ON

@if "%SETUPPG%x" == "0x" goto DNPG
@call addpg
@REM # You may need to manually set:
@REM #  PostgreSQL_INCLUDE_DIR  - the path to where the PostgreSQL include files are.
@REM #  PostgreSQL_LIBRARY_DIR  - The path to where the PostgreSQL library files are.
@REM set TMPOPTS=%TMPOPTS% "-DPostgreSQL_INCLUDE_DIR=C:\Program Files (x86)\PostgreSQL\9.1\include"
@REM set TMPOPTS=%TMPOPTS% "-DPostgreSQL_LIBRARY_DIR=C:\Program Files (x86)\PostgreSQL\9.1\lib"
@set PostgreSQL_ROOT=C:\Program Files (x86)\PostgreSQL\9.1
@echo Set ENV PostgreSQL_ROOT=%PostgreSQL_ROOT% >> %TMPLOG%
:DNPG

@echo Doing: 'cmake %TMPSRC% %TMPOPTS%'
@echo Doing: 'cmake %TMPSRC% %TMPOPTS%' >> %TMPLOG% 2>&1
@cmake %TMPSRC% %TMPOPTS% >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR1

@echo Doing 'cmake --build . --config Debug'
@echo Doing 'cmake --build . --config Debug' >> %TMPLOG% 2>&1
@cmake --build . --config Debug >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR2

@echo Doing: 'cmake --build . --config Release'
@echo Doing: 'cmake --build . --config Release' >> %TMPLOG% 2>&1
@cmake --build . --config Release >> %TMPLOG% 2>&1
@if ERRORLEVEL 1 goto ERR3

@fa4 "***" %TMPLOG%
@call elapsed %TMPBGN%
@echo Appears a successful build... see %TMPLOG%

@echo.
@echo At this moment INSTALL is excluded...
@echo.
@goto END

@if "%DOTMPINST%x" == "1x" goto DOTINST
@echo Built of artifact zips disabled...
@echo Built of artifact zips disabled... >>%TMPLOG%
@goto DNTINST
:DOTINST
@echo Building installation zips... moment...
@call build-zips Debug
@call build-zips Release
@echo Done installation zips...
:DNTINST

@if "%DOINSTALL%x" == "1x" goto DOINST
@echo Install is disabled...
@echo Install is disabled... >>%TMPLOG%
@goto DNINST

:DOINST
@echo Continue with install? Only Ctrl+c aborts...
@pause
@echo Doing: 'cmake --build . --config Debug  --target INSTALL'
@echo Doing: 'cmake --build . --config Debug  --target INSTALL' >> %TMPLOG% 2>&1
@cmake --build . --config Debug  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_dbg.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Debug install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)

@echo Doing: 'cmake --build . --config Release  --target INSTALL'
@echo Doing: 'cmake --build . --config Release  --target INSTALL' >> %TMPLOG% 2>&1
@cmake --build . --config Release  --target INSTALL >> %TMPLOG% 2>&1
@if EXIST install_manifest.txt (
@copy install_manifest.txt install_manifest_rel.txt >nul
@echo. >> %TMPINS%\installed.txt
@echo %TMPPRJ% Release install %DATE% %TIME% >> %TMPINS%\installed.txt
@type install_manifest.txt >> %TMPINS%\installed.txt
)
:DNINST

@call elapsed %TMPBGN%
@echo All done... see %TMPLOG%

@goto END

:NOSG
@echo NOT EXIST %TMPSG%\nul! *** FIX ME ***
@goto ISERR

:NOBST
@echo NOT EXIST %TMPBOOST%\nul! *** FIX ME ***
@goto ISERR

:ERR1
@echo cmake configuration or generations ERROR
@goto ISERR

:ERR2
@echo ERROR: Cmake build Debug FAILED!
@goto ISERR

:ERR3
@echo ERROR: Cmake build Release FAILED!
@goto ISERR

:ISERR
@echo See %TMPLOG% for details...
@endlocal
@exit /b 1

:END
@endlocal
@exit /b 0

@REM eof
