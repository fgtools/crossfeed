#!/bin/sh
#< run-local.sh - run crossfeed in local context

TMP_EXE="cf_client"
TMP_CFG="cf-local.ini"
TMP_LOG="cf_local.log"

if [ ! -f "$TMP_EXE" ]; then
    echo "$BN: Failed to find $TMP_EXE! *** FIX ME***"
    exit 1
fi

if [ ! -f "$TMP_CFG" ]; then
    echo "$BN: Failed to find $TMP_CFG! *** FIX ME***"
    exit 1
fi

if [ -f "$TMP_LOG" ]; then
    TMP_CNT=1
    TMP_NXT="$TMP_LOG.$TMP_CNT"
    while [ -f "$TMP_NXT" ]; do
        TMP_CNT=`expr $TMP_CNT + 1`
        TMP_NXT="$TMP_LOG.$TMP_CNT"
    done
    mv -v $TMP_LOG $TMP_NXT
    echo "$BN: renamed $TMP_LOG to $TMP_NXT"
fi

./$TMP_EXE -c $TMP_CFG

# eof

