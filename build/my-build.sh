#!/bin/sh
#< my-build.sh - my build preferences
###########################################
### WARNING: Just my chosen preferences ###
### Adjust to suit YOUR environment     ###
###########################################
TMPSG="/home/geoff/fg/next/install/simgear"
# TMPSG="/media/Disk2/FG/fg22/install/simgear"

if [ ! -d "$TMPSG" ]; then
    echo "Error: Oops, can not FIND $TMPSG! *** FIX ME ***"
    exit 1
fi


############# make sure to FIX this Simgear PATH #######
BLDOPS="ADDSG SGDIR=$TMPSG ADDMG DOCMAKE"
### Can NOT fid all PostgreSQL stuff, so
BLDOPS="$BLDOPS NOPGSQL"

./build-me.sh $BLDOPS $@

# eof

